require('../css/app.css');
require('@fortawesome/fontawesome-free/css/all.min.css');

var $ = require('jquery');

global.$ = global.jQuery = $

require('bootstrap');

const imagesContext = require.context('../images', true, /\.(png|jpg|jpeg|gif|ico|svg|webp)$/);
imagesContext.keys().forEach(imagesContext);
#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Install git (the php image doesn't have it) which is required by composer
echo "============================================================="
echo "==> Install git, zip, unzip and npm <=="
apt-get update -yqq
apt-get install git zip unzip npm -yqq
echo "==> End Install <=="

# Install composer to have a environment like local
echo "============================================================="
echo "==> Install composer <=="
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php
php -r "unlink('composer-setup.php');"
echo "==> End Install <=="

# Install phpunit, the tool that we will use for testing
# curl --location --output /usr/local/bin/phpunit https://phar.phpunit.de/phpunit.phar
# chmod +x /usr/local/bin/phpunit

# !!! Install mysql driver !!!
# Here you can install any other extension that you need
echo "============================================================="
echo "==> Install pdo and pdo_mysql through docker <=="
docker-php-ext-install pdo pdo_mysql
echo "==> End Install <=="

# Display version of npm and composer
echo "============================================================="
echo "==> Displaying version of NPM and composer <=="
npm -v
php composer.phar -V
echo "==> End displaying <=="
<?php

namespace App\Entity;

use Psr\Log\LoggerInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="App\Repository\TransactionRepository")
 */
class Transaction
{
    public const CATEGORY_PURCHASE = 'purchase';
    public const CATEGORY_SELL = 'sell';
    public const CATEGORY_PAYMENT = 'payment';
    public const CATEGORY_WITHDRAWAL = 'withdrawal';
    public const CATEGORY_DIVIDEND = 'dividend';
    public const CATEGORY_STOCKDIVIDEND = 'stockdividend';
    public const CATEGORY_FEE = 'fee';
    public const FEE_TTF = 'ttf';
    public const FEE_STAMPDUTY = 'stampduty';
    public const FEE_WITHHOLDING = 'withholding';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $category;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createAt;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $price;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $quantity;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Stock", inversedBy="transactions")
     * @ORM\JoinColumn(nullable=true)
     */
    private $stock;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\FinancialService", inversedBy="transactions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $financialService;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $debit;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $credit;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $amount;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $exchange;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $fee;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $feeCategory;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $brokerage;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $addon;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\LinePf", inversedBy="transactions")
     */
    private $linePf;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="transactions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    private $logger;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Portefolio", inversedBy="paymentsWithdrawalTr")
     */
    private $portefolio;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->logger->debug('Creating a transaction object.');
    }

    /**
     * Undocumented function
     *
     * @ORM\PrePersist
     * @return void
     */
    public function setPrePersistTransaction()
    {
        switch ($this->getCategory())
        {
            case $this::CATEGORY_PURCHASE: 
                $this->setAmount($this->price * $this->quantity);
                $this->setDebit(($this->getAmount() / ($this->exchange ?? 1)) + $this->getFee() + $this->getBrokerage());
            break;      
            case $this::CATEGORY_SELL: 
                $this->setAmount($this->price * $this->quantity);
                $this->setCredit(($this->getAmount() / ($this->exchange ?? 1)) - $this->getFee() - $this->getBrokerage());
            break;      
            case $this::CATEGORY_PAYMENT: 
                $this->setCredit($this->getAmount()); 
            break;      
            case $this::CATEGORY_WITHDRAWAL: 
                $this->setDebit($this->getAmount()); 
            break;      
            case $this::CATEGORY_DIVIDEND: 
                $this->setAmount($this->price * $this->quantity);
                $this->setCredit(($this->getAmount() - $this->getFee()) / ($this->exchange ?? 1));
            break;      
            case $this::CATEGORY_STOCKDIVIDEND: 
                $this->setAmount($this->price * $this->quantity);
                $this->setDebit($this->getAddon());
            break; 
            case $this::CATEGORY_FEE:
                $this->setDebit($this->getAmount());
            break;     
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getStock(): ?Stock
    {
        return $this->stock;
    }

    public function setStock(?Stock $stock): self
    {
        $this->stock = $stock;

        return $this;
    }

    public function getFinancialService(): ?FinancialService
    {
        return $this->financialService;
    }

    public function setFinancialService(?FinancialService $financialService): self
    {
        $this->financialService = $financialService;

        return $this;
    }

    public function getDebit(): ?float
    {
        return $this->debit;
    }

    public function setDebit(?float $debit): self
    {
        $this->debit = $debit;

        return $this;
    }

    public function getCredit(): ?float
    {
        return $this->credit;
    }

    public function setCredit(?float $credit): self
    {
        $this->credit = $credit;

        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(?float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getExchange(): ?float
    {
        return $this->exchange;
    }

    public function setExchange(?float $exchange): self
    {
        $this->exchange = $exchange;

        return $this;
    }

    public function getFee(): ?float
    {
        return $this->fee;
    }

    public function setFee(?float $fee): self
    {
        $this->fee = $fee;

        return $this;
    }

    public function getFeeCategory(): ?string
    {
        return $this->feeCategory;
    }

    public function setFeeCategory(?string $feeCategory): self
    {
        $this->feeCategory = $feeCategory;

        return $this;
    }

    public function getBrokerage(): ?float
    {
        return $this->brokerage;
    }

    public function setBrokerage(?float $brokerage): self
    {
        $this->brokerage = $brokerage;

        return $this;
    }

    public function getAddon(): ?float
    {
        return $this->addon;
    }

    public function setAddon(?float $addon): self
    {
        $this->addon = $addon;

        return $this;
    }

    public function getLinePf(): ?LinePf
    {
        return $this->linePf;
    }

    public function setLinePf(?LinePf $linePf): self
    {
        $this->linePf = $linePf;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getPortefolio(): ?Portefolio
    {
        return $this->portefolio;
    }

    public function setPortefolio(?Portefolio $portefolio): self
    {
        $this->portefolio = $portefolio;

        return $this;
    }
}

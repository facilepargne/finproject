<?php

namespace App\Entity;

use DateTime;
use App\Utils\YFParser;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="App\Repository\StockRepository")
 */
class Stock
{

    /**
     * Sector value : 'sector'
     */
    const SECTOR = 'SECTOR';
    const ACTIVITY_SECTOR = 'ACTIVITY_SECTOR';

    const SECTOR_ENERGY = 'Stocksector.energy';
    const SECTOR_MATERIALS = 'Stocksector.materials';
    const SECTOR_INDUSTRIALS = 'Stocksector.industrials';
    const SECTOR_FINANCIALS = 'Stocksector.financials';
    const SECTOR_CONSUMER_DISCRETIONARY = 'Stocksector.consumerdiscretionary';
    const SECTOR_TELECOM = 'Stocksector.telecom';
    const SECTOR_REAL_ESTATE = 'Stocksector.realestate';
    const SECTOR_CONSUMER_STAPLES = 'Stocksector.consumerstaples';
    const SECTOR_UTILITIES = 'Stocksector.utilities';
    const SECTOR_TECHNOLOGY = 'Stocksector.technology';
    const SECTOR_HEALTH_CARE = 'Stocksector.healthcare';
    const SECTOR_ETF = 'Stocksector.etf';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $shortName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $isinCode;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency", inversedBy="stocks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $currency;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Transaction", mappedBy="stock")
     */
    private $transactions;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sector;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $activitySector;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HistDataStock", mappedBy="stock")
     */
    private $histDataStocks;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $country;

    private $logger;
    private $loadSince;

    /**
     * Get Historical Data for stock
     *
     * @ORM\PrePersist
     * @return void
     */
    public function setHistData()
    {
        $start = microtime(true);

        $histData = YFParser::getHistDataFromStock($this, $this->loadSince, new DateTime());
        foreach ($histData as $key => $value) {
            $h = new HistDataStock();
            $h->setCreateAt(DateTime::createFromFormat('Y-m-d', $key))
                ->setOpen($value[0])
                ->setHigh($value[1])
                ->setLow($value[2])
                ->setClose($value[3])
                ->setAdjClose($value[4])
                ->setVolume($value[5]);
            $this->addHistDataStock($h);
        }

        $this->logger->debug('Fetching histdata for '.$this->getName().' in '.(microtime(true) - $start).' sec.');
    }

    public function __construct(LoggerInterface $logger)
    {
        $this->transactions = new ArrayCollection();
        $this->listHistoricalData = new ArrayCollection();
        $this->histDataStocks = new ArrayCollection();
        $this->logger = $logger;

        $this->logger->debug('Creating Stock object.');
    }

    public function setLoadSince($loadSince)
    {
        $this->loadSince = $loadSince;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getShortName(): ?string
    {
        return $this->shortName;
    }

    public function setShortName(string $shortName): self
    {
        $this->shortName = $shortName;

        return $this;
    }

    public function getIsinCode(): ?string
    {
        return $this->isinCode;
    }

    public function setIsinCode(string $isinCode): self
    {
        $this->isinCode = $isinCode;

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @return Collection|Transaction[]
     */
    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    public function addTransaction(Transaction $transaction): self
    {
        if (!$this->transactions->contains($transaction)) {
            $this->transactions[] = $transaction;
            $transaction->setStock($this);
        }

        return $this;
    }

    public function removeTransaction(Transaction $transaction): self
    {
        if ($this->transactions->contains($transaction)) {
            $this->transactions->removeElement($transaction);
            // set the owning side to null (unless already changed)
            if ($transaction->getStock() === $this) {
                $transaction->setStock(null);
            }
        }

        return $this;
    }

    public function getSector(): ?string
    {
        return $this->sector;
    }

    public function setSector(?string $sector): self
    {
        $this->sector = $sector;

        return $this;
    }

    public function getActivitySector(): ?string
    {
        return $this->activitySector;
    }

    public function setActivitySector(?string $activitySector): self
    {
        $this->activitySector = $activitySector;

        return $this;
    }

    /**
     * @return Collection|HistDataStock[]
     */
    public function getHistDataStocks(): Collection
    {
        return $this->histDataStocks;
    }

    public function addHistDataStock(HistDataStock $histDataStock): self
    {
        if (!$this->histDataStocks->contains($histDataStock)) {
            $this->histDataStocks[] = $histDataStock;
            $histDataStock->setStock($this);
        }

        return $this;
    }

    public function removeHistDataStock(HistDataStock $histDataStock): self
    {
        if ($this->histDataStocks->contains($histDataStock)) {
            $this->histDataStocks->removeElement($histDataStock);
            // set the owning side to null (unless already changed)
            if ($histDataStock->getStock() === $this) {
                $histDataStock->setStock(null);
            }
        }

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }
}

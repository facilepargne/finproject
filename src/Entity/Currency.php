<?php

namespace App\Entity;

use DateTime;
use App\Utils\YFParser;
use Psr\Log\LoggerInterface;
use App\Entity\HistDataCurrency;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="App\Repository\CurrencyRepository")
 */
class Currency
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $shortName;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $abbreviation;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Stock", mappedBy="currency")
     */
    private $stocks;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HistDataCurrency", mappedBy="currency")
     */
    private $histDataCurrencies;

    private $logger;
    private $loadSince;

    public function __construct(LoggerInterface $logger)
    {
        $this->stocks = new ArrayCollection();
        $this->histDataCurrencies = new ArrayCollection();
        $this->logger = $logger;
        $this->logger->debug('Creating currency object.');
    }
    
    /**
     * Get Historical Data for stock
     *
     * @ORM\PrePersist
     * @return void
     */
    public function setHistData()
    {
        $start = microtime(true);
        
        $histData = YFParser::getHistDataFromCurrency($this, $this->loadSince, new DateTime());
        
        foreach ($histData as $key => $value) {
            $h = new HistDataCurrency();
            $h->setCreateAt(DateTime::createFromFormat('Y-m-d', $key))
            ->setAdjClose($value);
            $this->addHistDataCurrency($h);
        }
        
        $this->logger->debug('Fetching histdata from currency in '.(microtime(true) - $start).' sec .');
    }

    public function setLoadSince(DateTime $loadSince)
    {
        $this->loadSince = $loadSince;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getShortName(): ?string
    {
        return $this->shortName;
    }

    public function setShortName(string $shortName): self
    {
        $this->shortName = $shortName;

        return $this;
    }

    public function getAbbreviation(): ?string
    {
        return $this->abbreviation;
    }

    public function setAbbreviation(string $abbreviation): self
    {
        $this->abbreviation = $abbreviation;

        return $this;
    }

    /**
     * @return Collection|Stock[]
     */
    public function getStocks(): Collection
    {
        return $this->stocks;
    }

    public function addStock(Stock $stock): self
    {
        if (!$this->stocks->contains($stock)) {
            $this->stocks[] = $stock;
            $stock->setCurrency($this);
        }

        return $this;
    }

    public function removeStock(Stock $stock): self
    {
        if ($this->stocks->contains($stock)) {
            $this->stocks->removeElement($stock);
            // set the owning side to null (unless already changed)
            if ($stock->getCurrency() === $this) {
                $stock->setCurrency(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HistDataCurrency[]
     */
    public function getHistDataCurrencies(): Collection
    {
        return $this->histDataCurrencies;
    }

    public function addHistDataCurrency(HistDataCurrency $histDataCurrency): self
    {
        if (!$this->histDataCurrencies->contains($histDataCurrency)) {
            $this->histDataCurrencies[] = $histDataCurrency;
            $histDataCurrency->setCurrency($this);
        }

        return $this;
    }

    public function removeHistDataCurrency(HistDataCurrency $histDataCurrency): self
    {
        if ($this->histDataCurrencies->contains($histDataCurrency)) {
            $this->histDataCurrencies->removeElement($histDataCurrency);
            // set the owning side to null (unless already changed)
            if ($histDataCurrency->getCurrency() === $this) {
                $histDataCurrency->setCurrency(null);
            }
        }

        return $this;
    }
}

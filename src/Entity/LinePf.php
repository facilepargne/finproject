<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LinePfRepository")
 */
class LinePf
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Stock")
     * @ORM\JoinColumn(nullable=false)
     */
    private $stock;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\Column(type="float")
     */
    private $sharePrice;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Transaction", mappedBy="linePf")
     */
    private $transactions;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Portefolio", inversedBy="linesPf")
     * @ORM\JoinColumn(nullable=false)
     */
    private $portefolio;

    /**
     * @ORM\Column(type="boolean")
     */
    private $status;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $amount;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $ucp;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $latentGl;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $latentGlP;

    public function __construct()
    {
        $this->transactions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStock(): ?Stock
    {
        return $this->stock;
    }

    public function setStock(?Stock $stock): self
    {
        $this->stock = $stock;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getSharePrice(): ?float
    {
        return $this->sharePrice;
    }

    public function setSharePrice(float $sharePrice): self
    {
        $this->sharePrice = $sharePrice;

        return $this;
    }

    /**
     * @return Collection|Transaction[]
     */
    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    public function addTransaction(Transaction $transaction): self
    {
        if (!$this->transactions->contains($transaction)) {
            $this->transactions[] = $transaction;
            $transaction->setLinePf($this);
        }

        return $this;
    }

    public function removeTransaction(Transaction $transaction): self
    {
        if ($this->transactions->contains($transaction)) {
            $this->transactions->removeElement($transaction);
            // set the owning side to null (unless already changed)
            if ($transaction->getLinePf() === $this) {
                $transaction->setLinePf(null);
            }
        }

        return $this;
    }

    public function getPortefolio(): ?Portefolio
    {
        return $this->portefolio;
    }

    public function setPortefolio(?Portefolio $portefolio): self
    {
        $this->portefolio = $portefolio;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(?float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getUcp(): ?float
    {
        return $this->ucp;
    }

    public function setUcp(?float $ucp): self
    {
        $this->ucp = $ucp;

        return $this;
    }

    public function getLatentGl(): ?float
    {
        return $this->latentGl;
    }

    public function setLatentGl(?float $latentGl): self
    {
        $this->latentGl = $latentGl;

        return $this;
    }

    public function getLatentGlP(): ?float
    {
        return $this->latentGlP;
    }

    public function setLatentGlP(?float $latentGlP): self
    {
        $this->latentGlP = $latentGlP;

        return $this;
    }
}

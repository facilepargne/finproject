<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HistDataPortefolioRepository")
 */
class HistDataPortefolio
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createAt;

    /**
     * @ORM\Column(type="float")
     */
    private $valuation;

    /**
     * @ORM\Column(type="float")
     */
    private $cash;

    /**
     * @ORM\Column(type="float")
     */
    private $payments;

    /**
     * @ORM\Column(type="float")
     */
    private $xirr;

    /**
     * @ORM\Column(type="float")
     */
    private $globalXirr;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Portefolio", inversedBy="histDataPortefolios")
     * @ORM\JoinColumn(nullable=false)
     */
    private $portefolio;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getValuation(): ?float
    {
        return $this->valuation;
    }

    public function setValuation(float $valuation): self
    {
        $this->valuation = $valuation;

        return $this;
    }

    public function getCash(): ?float
    {
        return $this->cash;
    }

    public function setCash(float $cash): self
    {
        $this->cash = $cash;

        return $this;
    }

    public function getPayments(): ?float
    {
        return $this->payments;
    }

    public function setPayments(float $payments): self
    {
        $this->payments = $payments;

        return $this;
    }

    public function getXirr(): ?float
    {
        return $this->xirr;
    }

    public function setXirr(float $xirr): self
    {
        $this->xirr = $xirr;

        return $this;
    }

    public function getGlobalXirr(): ?float
    {
        return $this->globalXirr;
    }

    public function setGlobalXirr(float $globalXirr): self
    {
        $this->globalXirr = $globalXirr;

        return $this;
    }

    public function getPortefolio(): ?Portefolio
    {
        return $this->portefolio;
    }

    public function setPortefolio(?Portefolio $portefolio): self
    {
        $this->portefolio = $portefolio;

        return $this;
    }
}

<?php

namespace App\Entity;

use DateTime;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PortefolioRepository")
 */
class Portefolio
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\FinancialService", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $fs;

    /**
     * @ORM\Column(type="float")
     */
    private $cash;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="portefolios")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LinePf", mappedBy="portefolio")
     */
    private $linesPf;

    /**
     * @ORM\Column(type="float")
     */
    private $payments;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HistDataPortefolio", mappedBy="portefolio")
     */
    private $histDataPortefolios;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $valuation;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $xirr;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $globalXirr;

    private $logger;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Transaction", mappedBy="portefolio")
     */
    private $paymentsWithdrawalTr;

    /**
     * @ORM\Column(type="date")
     */
    private $lastValuation;

    public function __construct(DateTime $createAt, LoggerInterface $logger)
    {
        $this->linesPf = new ArrayCollection();
        $this->histDataPortefolios = new ArrayCollection();
        $this->paymentsWithdrawalTr = new ArrayCollection();
        $this->cash = 0;
        $this->createAt = $createAt;
        $this->lastValuation = $createAt;
        $this->payments = 0;
        $this->valuation = 0;
        $this->xirr = 0;
        $this->globalXirr = 0;
        $this->logger = $logger;
        $this->logger->debug('Creating portefolio object.');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFs(): ?FinancialService
    {
        return $this->fs;
    }

    public function setFs(FinancialService $fs): self
    {
        $this->fs = $fs;

        return $this;
    }

    public function getCash(): ?float
    {
        return $this->cash;
    }

    public function setCash(float $cash): self
    {
        $this->cash = $cash;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|LinePf[]
     */
    public function getLinesPf(): Collection
    {
        return $this->linesPf;
    }

    public function addLinesPf(LinePf $linesPf): self
    {
        if (!$this->linesPf->contains($linesPf)) {
            $this->linesPf[] = $linesPf;
            $linesPf->setPortefolio($this);
        }

        return $this;
    }

    public function removeLinesPf(LinePf $linesPf): self
    {
        if ($this->linesPf->contains($linesPf)) {
            $this->linesPf->removeElement($linesPf);
            // set the owning side to null (unless already changed)
            if ($linesPf->getPortefolio() === $this) {
                $linesPf->setPortefolio(null);
            }
        }

        return $this;
    }

    public function getPayments(): ?float
    {
        return $this->payments;
    }

    public function setPayments(float $payments): self
    {
        $this->payments = $payments;

        return $this;
    }

    /**
     * @return Collection|HistDataPortefolio[]
     */
    public function getHistDataPortefolios(): Collection
    {
        return $this->histDataPortefolios;
    }

    public function addHistDataPortefolio(HistDataPortefolio $histDataPortefolio): self
    {
        if (!$this->histDataPortefolios->contains($histDataPortefolio)) {
            $this->histDataPortefolios[] = $histDataPortefolio;
            $histDataPortefolio->setPortefolio($this);
        }

        return $this;
    }

    public function removeHistDataPortefolio(HistDataPortefolio $histDataPortefolio): self
    {
        if ($this->histDataPortefolios->contains($histDataPortefolio)) {
            $this->histDataPortefolios->removeElement($histDataPortefolio);
            // set the owning side to null (unless already changed)
            if ($histDataPortefolio->getPortefolio() === $this) {
                $histDataPortefolio->setPortefolio(null);
            }
        }

        return $this;
    }

    public function getValuation(): ?float
    {
        return $this->valuation;
    }

    public function setValuation(?float $valuation): self
    {
        $this->valuation = $valuation;

        return $this;
    }

    public function getXirr(): ?float
    {
        return $this->xirr;
    }

    public function setXirr(?float $xirr): self
    {
        $this->xirr = $xirr;

        return $this;
    }

    public function getGlobalXirr(): ?float
    {
        return $this->globalXirr;
    }

    public function setGlobalXirr(?float $globalXirr): self
    {
        $this->globalXirr = $globalXirr;

        return $this;
    }

    /**
     * @return Collection|Transaction[]
     */
    public function getPaymentsWithdrawalTr(): Collection
    {
        return $this->paymentsWithdrawalTr;
    }

    public function addPaymentsWithdrawalTr(Transaction $paymentsWithdrawalTr): self
    {
        if (!$this->paymentsWithdrawalTr->contains($paymentsWithdrawalTr)) {
            $this->paymentsWithdrawalTr[] = $paymentsWithdrawalTr;
            $paymentsWithdrawalTr->setPortefolio($this);
        }

        return $this;
    }

    public function removePaymentsWithdrawalTr(Transaction $paymentsWithdrawalTr): self
    {
        if ($this->paymentsWithdrawalTr->contains($paymentsWithdrawalTr)) {
            $this->paymentsWithdrawalTr->removeElement($paymentsWithdrawalTr);
            // set the owning side to null (unless already changed)
            if ($paymentsWithdrawalTr->getPortefolio() === $this) {
                $paymentsWithdrawalTr->setPortefolio(null);
            }
        }

        return $this;
    }

    public function getLastValuation(): ?\DateTimeInterface
    {
        return $this->lastValuation;
    }

    public function setLastValuation(\DateTimeInterface $lastValuation): self
    {
        $this->lastValuation = $lastValuation;

        return $this;
    }
}

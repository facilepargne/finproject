<?php

namespace App\DataFixtures;

use DateTime;
use App\Entity\User;
use App\Entity\Stock;
use App\Utils\YFParser;
use App\Entity\Currency;
use App\Entity\Portefolio;
use App\Entity\Transaction;
use Psr\Log\LoggerInterface;
use App\Entity\Establishment;
use App\Entity\HistDataStock;
use App\Entity\FinancialService;
use App\Entity\HistDataCurrency;
use App\Utils\PortefolioFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    /**
     * List of currencies
     *
     * @var array
     */
    private $currencies = [];

    /**
     * List of stocks
     *
     * @var array
     */
    private $stocks = [];

    /**
     * List of establishments
     *
     * @var array
     */
    private $establishments = [];

    /**
     * List of financial services
     *
     * @var array
     */
    private $financialServices = [];

    /**
     * List of transactions
     *
     * @var array
     */
    private $transactions = [];

    /**
     * List of users
     *
     * @var array
     */
    private $users = [];

    /**
     * List of portefolios
     *
     * @var array
     */
    private $portefolios = [];

    /**
     * Password encoder
     *
     * @var [type]
     */
    private $passwordEncoder;

    private $logger;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, LoggerInterface $logger)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->logger = $logger;
    }

    /**
     * Load all object into database
     *
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        $start = microtime(true);
        $this->logger->debug('========= Loading fixtures ! ==========');

        $this->loadUsers($manager);
        $this->loadCurrencies($manager);
        $this->loadStocks($manager);
        $this->loadEstablishments($manager);
        $this->loadFinancialService($manager);
        // $this->loadPortefolios($manager);
        $this->loadRealTransactions($manager);
    }

    /**
     * Load some currencies
     *
     * @param ObjectManager $manager
     * @return void
     */
    private function loadCurrencies(ObjectManager $manager) 
    {
        $start = microtime(true);
        
        $this->loadOneCurrency($manager, 'Euro', 'EUR', '€');
        $this->loadOneCurrency($manager, 'United States Dollar', 'USD', '$');
        $this->loadOneCurrency($manager, 'Great Britain Pound', 'GBP', '£');
        $this->loadOneCurrency($manager, 'Franc suisse', 'CHF', 'CHF');
        
        $manager->flush();
        $this->logger->debug('====>> Loading all currencies in '.(microtime(true) - $start).' sec !');
    }

    /**
     * Load One currency with name, shortName and abbreviation. If hist is true, load currency historical data.
     *
     * @param ObjectManager $manager
     * @param string $name
     * @param string $shortName
     * @param string $abbreviation
     * @param boolean $hist
     * @return void
     */
    private function loadOneCurrency(ObjectManager $manager, string $name, string $shortName, string $abbreviation, bool $hist = true)
    {
        $currency = new Currency($this->logger);
        $currency->setName($name)
                 ->setShortName($shortName)
                 ->setAbbreviation($abbreviation)
                 ->setLoadSince(DateTime::createFromFormat('Y-m-d', '2018-04-27')->setTime(0, 0));

        array_push($this->currencies, $currency);

        $manager->persist($currency);

        foreach ($currency->getHistDataCurrencies() as $key => $value) {
            $value->setCurrency($currency);
            $manager->persist($value);
        }

        $this->logger->debug('Loading '.$name.' ('.$shortName.') with '.count($currency->getHistDataCurrencies()).' historical data.');
    }

    /**
     * Load some stocks
     *
     * @param ObjectManager $manager
     * @return void
     */
    private function loadStocks(ObjectManager $manager)
    {
        $start = microtime(true);

        $this->loadOneStock($manager, "VINCI", "DG.PA", "FR0000125486", Stock::SECTOR_INDUSTRIALS, 'Bâtiments et matériaux de construction'); // 0
        $this->loadOneStock($manager, "AIR LIQUIDE", "AI.PA", "FR0000120073", Stock::SECTOR_MATERIALS); // 1
        $this->loadOneStock($manager, "SOCIETE GENERALE", "GLE.PA", "FR0000130809", Stock::SECTOR_FINANCIALS); // 2
        $this->loadOneStock($manager, "L'OREAL", "OR.PA", "FR0000120321", Stock::SECTOR_CONSUMER_STAPLES, 'Articles personnels'); // 3
        $this->loadOneStock($manager, "LVMH", "MC.PA", "FR0000121014", Stock::SECTOR_CONSUMER_DISCRETIONARY, 'Articles personnels'); // 4
        $this->loadOneStock($manager, "TOTAL", "FP.PA", "FR0000120271", Stock::SECTOR_ENERGY); // 5
        $this->loadOneStock($manager, "MICROSOFT", "MSFT", "US5949181045", Stock::SECTOR_TECHNOLOGY, 'Logiciels et services informatique', 'US',1); // 6
        $this->loadOneStock($manager, "AXA", "CS.PA", "FR0000120628", Stock::SECTOR_FINANCIALS, 'Assurances'); // 7
        $this->loadOneStock($manager, "PIXIUM VISION", "PIX.PA", "FR0011950641", Stock::SECTOR_HEALTH_CARE); // 8
        $this->loadOneStock($manager, "DANONE", "BN.PA", "FR0000120644", Stock::SECTOR_CONSUMER_STAPLES, 'Agro-alimentaire'); // 9
        $this->loadOneStock($manager, "AT&T", "T", "US00206R1023", Stock::SECTOR_TELECOM, '', 'US', 1); // 10
        $this->loadOneStock($manager, "BOUYGUES", "EN.PA", "FR0000120503", Stock::SECTOR_INDUSTRIALS, 'Bâtiments et matériaux de construction'); // 11
        $this->loadOneStock($manager, "AVENIR TELECOM", "AVT.PA", "FR0000066052", Stock::SECTOR_TELECOM); // 12
        $this->loadOneStock($manager, "OENEO", "SBT.PA", "FR0000052680", Stock::SECTOR_INDUSTRIALS); // 13
        $this->loadOneStock($manager, "RENAULT", "RNO.PA", "FR0000131906", Stock::SECTOR_CONSUMER_DISCRETIONARY, 'Automobiles et équipementiers'); // 14
        $this->loadOneStock($manager, "LYXOR PEA S&P 500", "PSP5.PA", "FR0011871128", Stock::SECTOR_ETF, '', 'US'); // 15
        $this->loadOneStock($manager, "AMUNDI ETF EUR MID", "CEM.PA", "LU1681041544", Stock::SECTOR_ETF, '', 'ETF'); // 16
        $this->loadOneStock($manager, "BNPP STOXX 600C", "ETSZ.DE", "FR0011550193", Stock::SECTOR_ETF, '', 'ETF'); // 17
        $this->loadOneStock($manager, "LYXOR HSCEI PEA", "PASI.PA", "FR0011871078", Stock::SECTOR_ETF, '', 'CH'); // 18
        $this->loadOneStock($manager, "AMUNDI ETF MSCI EM", "AMEM.F", "LU1681045370", Stock::SECTOR_ETF, '', 'ETF'); // 19
        $this->loadOneStock($manager, "AMUNDI RUSSEL 2K", "RS2K.PA", "LU1681038672", Stock::SECTOR_ETF, '', 'US'); // 20
        $this->loadOneStock($manager, "AMUNDI MSCI WORLD", "AMEW.F", "LU1681043599", Stock::SECTOR_ETF, '', 'ETF'); // 21
        $this->loadOneStock($manager, "CISCO", "CSCO", "US17275R1023", Stock::SECTOR_TECHNOLOGY, 'Matériel et équipements informatiques', 'United States', 1); // 22
        $this->loadOneStock($manager, "AMUNDI TOPIX EUR", "TPXE.PA", "LU1681037609", Stock::SECTOR_ETF, '', 'JP'); // 23
        $this->loadOneStock($manager, "Amundi CAC 40 UCITS", "C40.PA", "LU1681046931", Stock::SECTOR_ETF, '', 'FR'); // 24
        
        $this->logger->debug('====>> Loading all stocks in '.(microtime(true) - $start).' sec.');
    }
    
    private function loadOneStock(ObjectManager $manager, string $name, string $shortName, string $isin, string $sector = null, 
        string $activitySector = null, string $country = 'FR', int $indexCurrency = 0, bool $hist = true)
    {
        $stock = new Stock($this->logger);
        $stock->setName($name)
              ->setShortName($shortName)
              ->setIsinCode($isin)
              ->setCurrency($this->currencies[$indexCurrency])
              ->setCountry($country)
              ->setLoadSince(DateTime::createFromFormat('Y-m-d', '2018-04-27')->setTime(0, 0));
        
        if (!is_null($sector))
        {
            $stock->setSector($sector);
        }
        
        array_push($this->stocks, $stock);
        
        $manager->persist($stock);

        foreach ($stock->getHistDataStocks() as $key => $value) {
            $value->setStock($stock);
            $manager->persist($value);
        }

        $manager->flush();

        $this->logger->debug('Loading '.$name.' ('.$stock->getCurrency()->getAbbreviation().') with '
            .count($stock->getHistDataStocks()).' historical data.');
    }

    /**
     * Load some establishments
     *
     * @param ObjectManager $manager
     * @return void
     */
    private function loadEstablishments(ObjectManager $manager)
    {
        $start = microtime(true);
        
        $establishmentBoursorama = new Establishment($this->logger);
        $establishmentDegiro = new Establishment($this->logger);

        $establishmentBoursorama->setName('Boursorama')
                                ->setGenre('Bank');

        $establishmentDegiro->setName('Degiro')
                            ->setGenre('Broker');

        array_push($this->establishments, $establishmentBoursorama);
        array_push($this->establishments, $establishmentDegiro);
        
        foreach($this->establishments as $establishment)
        {
            $manager->persist($establishment);
            $this->logger->debug('Establishment : '.$establishment->getName().'.');
        }
        
        $manager->flush();
        
        $this->logger->debug('====>> Loading '.count($this->establishments).' establishments in '.(microtime(true) - $start).' sec.');
    }
    
    /**
     * Load some financial services
     *
     * @param ObjectManager $manager
     * @return void
     */
    private function loadFinancialService(ObjectManager $manager)
    {
        $start = microtime(true);
        $fsPea = new FinancialService($this->logger);
        $fsCto = new FinancialService($this->logger);
        
        $fsPea->setName('PEA Perso')
        ->setGenre('PEA')
        ->setCreateAt(DateTime::createFromFormat('d-m-Y', '28-05-2018'))
        ->setBalance(0)
        ->setUser($this->users[0]);
        
        $fsCto->setName('CTO - Degiro')
        ->setGenre('CTO')
        ->setCreateAt(DateTime::createFromFormat('d-m-Y', '27-04-2018'))
        ->setBalance(0)
        ->setUser($this->users[0]);
        
        array_push($this->financialServices, $fsPea);
        array_push($this->financialServices, $fsCto);
        
        $this->financialServices[0]->setEstablishment($this->establishments[0]);
        $this->financialServices[1]->setEstablishment($this->establishments[1]);
        
        $this->logger->debug('Financial Service : '.$this->financialServices[0]->getName().'.');
        $this->logger->debug('Financial Service : '.$this->financialServices[1]->getName().'.');

        $portefolioCTO = new Portefolio(DateTime::createFromFormat('Y-m-d', $fsCto->getCreateAt()->format('Y-m-d'))->setTime(0, 0), $this->logger);
        $portefolioCTO->setFs($fsCto)
                      ->setUser($this->users[0]);
        $portefolioPEA = new Portefolio(DateTime::createFromFormat('Y-m-d', $fsPea->getCreateAt()->format('Y-m-d'))->setTime(0, 0), $this->logger);
        $portefolioPEA->setFs($fsPea)
                      ->setUser($this->users[0]);
        array_push($this->portefolios, $portefolioCTO);
        array_push($this->portefolios, $portefolioPEA);

        $manager->persist($this->financialServices[0]);
        $manager->persist($this->financialServices[1]);
        $manager->persist($portefolioCTO);
        $manager->persist($portefolioPEA);

        $manager->flush();
        
        $this->logger->debug('====>> Loading '.count($this->financialServices).' establishments in '.(microtime(true) - $start).' sec.');
    }

    /**
     * Load portefolios into database
     *
     * @param ObjectManager $manager
     * @return void
     */
    private function loadPortefolios(ObjectManager $manager)
    {
        $start = microtime(true);

        $pCTO = new Portefolio(DateTime::createFromFormat('Y-m-d', '2018-04-27')->setTime(0, 0), $this->logger);
        $pPEA = new Portefolio(DateTime::createFromFormat('Y-m-d', '2018-05-28')->setTime(0, 0), $this->logger);

        $pCTO->setUser($this->users[0])
            ->setFs($this->financialServices[0]);

        $pPEA->setUser($this->users[0])
            ->setFs($this->financialServices[1]);

        $manager->persist($pCTO);
        $manager->persist($pPEA);

        $manager->flush();

        array_push($this->portefolios, $pCTO);
        array_push($this->portefolios, $pPEA);

        $this->logger->debug('====>> Loading portefolios in '.(microtime(true) - $start).' sec.');
    }
    
    /**
     * Load some transactions
     *
     * @param ObjectManager $manager
     * @return void
     */
    private function loadRealTransactions(ObjectManager $manager)
    {
        $start = microtime(true);
        // CTO
        $this->loadPaymentWithdrawal($manager, $this->portefolios[0], Transaction::CATEGORY_PAYMENT, '27-04-2018', 1, 50, "First payment");
        $this->loadPurchaseSellDividend($manager, $this->portefolios[0], Transaction::CATEGORY_PURCHASE, '27-04-2018', 1, 23.47, 2, 7, 0.02, 0.14, Transaction::FEE_TTF, 0.0, "Fist Investment ! :-)");
        $this->loadPaymentWithdrawal($manager, $this->portefolios[0], Transaction::CATEGORY_PAYMENT, '04-05-2018', 1, 30);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[0], Transaction::CATEGORY_PURCHASE, '04-05-2018', 1, 2.175, 15, 8, 0.01);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[0], Transaction::CATEGORY_DIVIDEND, '07-05-2018', 1, 1.26, 2, 7, 0.0, 0.76, Transaction::FEE_WITHHOLDING);
        $this->loadPaymentWithdrawal($manager, $this->portefolios[0], Transaction::CATEGORY_PAYMENT, '07-05-2018', 1, 70);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[0], Transaction::CATEGORY_PURCHASE, '11-05-2018', 1, 64.8, 1, 9, 0.03, 0.19, Transaction::FEE_TTF);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[0], Transaction::CATEGORY_PURCHASE, '16-05-2018', 1, 2.075, 3, 8);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[0], Transaction::CATEGORY_SELL, '25-05-2018', 1, 1.99, 18, 8, 0.01);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[0], Transaction::CATEGORY_PURCHASE, '25-05-2018', 1, 32.66, 1, 10, 0.5, 0.0, "", 1.1643);
        $this->loadPaymentWithdrawal($manager, $this->portefolios[0], Transaction::CATEGORY_FEE, '31-05-2018', 1, 0.37, "Frais de connexion à NYSE en 2018");
        $this->loadPurchaseSellDividend($manager, $this->portefolios[0], Transaction::CATEGORY_PURCHASE, '15-06-2018', 1, 0.404, 18, 12);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[0], Transaction::CATEGORY_SELL, '10-07-2018', 1, 0.231, 18, 12);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[0], Transaction::CATEGORY_SELL, '10-07-2018', 1, 64.5, 1, 9, 0.03);
        $this->loadPaymentWithdrawal($manager, $this->portefolios[0], Transaction::CATEGORY_PAYMENT, '11-07-2018', 1, 75);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[0], Transaction::CATEGORY_SELL, '11-07-2018', 1, 20.7, 2, 7, 0.02);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[0], Transaction::CATEGORY_PURCHASE, '11-07-2018', 1, 101.2, 2, 6, 0.51, 0.0, "", 1.1737);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[0], Transaction::CATEGORY_DIVIDEND, '01-08-2018', 1, 0.5, 1, 10, 0.0, 0.08, Transaction::FEE_WITHHOLDING, 1.1672);
        $this->loadPaymentWithdrawal($manager, $this->portefolios[0], Transaction::CATEGORY_FEE, '03-08-2018', 1, 0.77, "Frais de connexion à NYSE et NASDAQ en 2018");
        $this->loadPaymentWithdrawal($manager, $this->portefolios[0], Transaction::CATEGORY_FEE, '05-09-2018', 1, 0.02, "Frais de connexion à NYSE et NASDAQ en 2018");
        $this->loadPurchaseSellDividend($manager, $this->portefolios[0], Transaction::CATEGORY_DIVIDEND, '13-09-2018', 1, 0.42, 2, 6, 0.0, 0.13, Transaction::FEE_WITHHOLDING, 1.1702);
        $this->loadPaymentWithdrawal($manager, $this->portefolios[0], Transaction::CATEGORY_FEE, '01-10-2018', 1, 0.04, "Frais de connexion à NYSE et NASDAQ en 2018");
        $this->loadPurchaseSellDividend($manager, $this->portefolios[0], Transaction::CATEGORY_DIVIDEND, '01-11-2018', 1, 0.5, 1, 10, 0.0, 0.08, Transaction::FEE_WITHHOLDING, 1.142);
        $this->loadPaymentWithdrawal($manager, $this->portefolios[0], Transaction::CATEGORY_FEE, '02-11-2018', 1, 0.02, "Frais de connexion à NYSE et NASDAQ en 2018");
        $this->loadPurchaseSellDividend($manager, $this->portefolios[0], Transaction::CATEGORY_DIVIDEND, '14-12-2018', 1, 0.46, 2, 6, 0.0, 0.14, Transaction::FEE_WITHHOLDING, 1.1317);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[0], Transaction::CATEGORY_DIVIDEND, '01-02-2019', 1, 0.51, 1, 10, 0.0, 0.08, Transaction::FEE_WITHHOLDING, 1.1468);
        $this->loadPaymentWithdrawal($manager, $this->portefolios[0], Transaction::CATEGORY_FEE, '01-02-2019', 1, 1.16, "Frais de connexion à NYSE et NASDAQ en 2019");
        $this->loadPaymentWithdrawal($manager, $this->portefolios[0], Transaction::CATEGORY_FEE, '04-03-2019', 1, 0.02, "Frais de connexion à NYSE et NASDAQ en 2019");
        $this->loadPurchaseSellDividend($manager, $this->portefolios[0], Transaction::CATEGORY_DIVIDEND, '14-03-2019', 1, 0.46, 2, 6, 0.0, 0.14, Transaction::FEE_WITHHOLDING, 1.1316);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[0], Transaction::CATEGORY_SELL, '19-03-2019', 1, 31, 1, 10, 0.5, 0.0, "", 1.1359);
        $this->loadPaymentWithdrawal($manager, $this->portefolios[0], Transaction::CATEGORY_FEE, '01-04-2019', 1, 0.08, "Frais de connexion à NYSE et NASDAQ en 2019");
        $this->loadPaymentWithdrawal($manager, $this->portefolios[0], Transaction::CATEGORY_FEE, '03-05-2019', 1, 0.10, "Frais de connexion à NYSE et NASDAQ en 2019");
        $this->loadPurchaseSellDividend($manager, $this->portefolios[0], Transaction::CATEGORY_DIVIDEND, '13-06-2019', 1, 0.46, 2, 6, 0.0, 0.14, Transaction::FEE_WITHHOLDING, 1.1288);
        $this->loadPaymentWithdrawal($manager, $this->portefolios[0], Transaction::CATEGORY_FEE, '03-07-2019', 1, 0.06, "Frais de connexion à NYSE et NASDAQ en 2019");
        $this->loadPaymentWithdrawal($manager, $this->portefolios[0], Transaction::CATEGORY_FEE, '01-08-2019', 1, 0.06, "Frais de connexion à NYSE et NASDAQ en 2019");
        $this->loadPurchaseSellDividend($manager, $this->portefolios[0], Transaction::CATEGORY_DIVIDEND, '12-09-2019', 1, 0.46, 2, 6, 0.0, 0.14, Transaction::FEE_WITHHOLDING, 1.1074);
        $this->loadPaymentWithdrawal($manager, $this->portefolios[0], Transaction::CATEGORY_PAYMENT, '16-09-2019', 1, 0.01, "Retenue à la source AT&T corrigée 2018");
        $this->loadPaymentWithdrawal($manager, $this->portefolios[0], Transaction::CATEGORY_FEE, '01-11-2019', 1, 0.02, "Frais de connexion à NYSE et NASDAQ en 2019");
        $this->loadPaymentWithdrawal($manager, $this->portefolios[0], Transaction::CATEGORY_FEE, '02-12-2019', 1, 0.10, "Frais de connexion à NYSE et NASDAQ en 2019");
        $this->loadPurchaseSellDividend($manager, $this->portefolios[0], Transaction::CATEGORY_DIVIDEND, '12-12-2019', 1, 0.51, 2, 6, 0.0, 0.15, Transaction::FEE_WITHHOLDING, 1.1141);
        $this->loadPaymentWithdrawal($manager, $this->portefolios[0], Transaction::CATEGORY_FEE, '31-12-2019', 1, 0.04, "Frais de connexion à NYSE et NASDAQ en 2019 - date réelle de l'opération : 02-01-2020");
        $this->loadPaymentWithdrawal($manager, $this->portefolios[0], Transaction::CATEGORY_FEE, '03-02-2020', 1, 0.89, "Frais de connexion à NASDAQ en 2020");
        $this->loadPaymentWithdrawal($manager, $this->portefolios[0], Transaction::CATEGORY_FEE, '01-03-2020', 1, 0.08, "Frais de connexion à NASDAQ en 2020");
        $this->loadPurchaseSellDividend($manager, $this->portefolios[0], Transaction::CATEGORY_DIVIDEND, '12-03-2020', 1, 0.51, 2, 6, 0.0, 0.15, Transaction::FEE_WITHHOLDING, 1.1196);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[0], Transaction::CATEGORY_PURCHASE, '15-04-2020', 1, 41.48, 1, 22, 0.5, 0.0, "", 1.0905);
        $this->loadPaymentWithdrawal($manager, $this->portefolios[0], Transaction::CATEGORY_PAYMENT, '17-04-2020', 1, 1);

        // PEA
        $this->loadPaymentWithdrawal($manager, $this->portefolios[1], Transaction::CATEGORY_PAYMENT, '28-05-2018', 0, 90, "Date fiscale");
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_PURCHASE, '29-05-2018', 0, 40, 2, 11, 1.99, 0.24, Transaction::FEE_TTF);
        $this->loadPaymentWithdrawal($manager, $this->portefolios[1], Transaction::CATEGORY_PAYMENT, '02-07-2018', 0, 150);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_PURCHASE, '02-07-2018', 0, 9.76, 15, 13, 1.99);
        $this->loadPaymentWithdrawal($manager, $this->portefolios[1], Transaction::CATEGORY_PAYMENT, '30-07-2018', 0, 150);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_PURCHASE, '02-08-2018', 0, 72.93, 2, 14, 1.99, 0.44, Transaction::FEE_TTF);
        $this->loadStockDividend($manager, $this->portefolios[1], Transaction::CATEGORY_STOCKDIVIDEND, '03-08-2018', 0, 8.95, 1, 13, 6.7);
        $this->loadPaymentWithdrawal($manager, $this->portefolios[1], Transaction::CATEGORY_PAYMENT, '29-08-2018', 0, 160);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_PURCHASE, '30-08-2018', 0, 19.659, 8, 15, 1.99);
        $this->loadPaymentWithdrawal($manager, $this->portefolios[1], Transaction::CATEGORY_PAYMENT, '30-11-2018', 0, 200);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_PURCHASE, '30-11-2018', 0, 48.65, 4, 5, 1.99, 0.58, Transaction::FEE_TTF);
        $this->loadPaymentWithdrawal($manager, $this->portefolios[1], Transaction::CATEGORY_PAYMENT, '09-01-2019', 0, 175);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_PURCHASE, '09-01-2019', 0, 19.4, 9, 7, 1.99, 0.52, Transaction::FEE_TTF);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_DIVIDEND, '10-01-2019', 0, 0.64, 4, 5);
        $this->loadPaymentWithdrawal($manager, $this->portefolios[1], Transaction::CATEGORY_PAYMENT, '26-02-2019', 0, 200);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_PURCHASE, '28-02-2019', 0, 94.0908, 2, 16, 1.99);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_SELL, '05-03-2019', 0, 22.46, 9, 7, 1.99);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_PURCHASE, '05-03-2019', 0, 9.998, 21, 17, 1.99);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_SELL, '07-03-2019', 0, 50.9, 4, 5, 1.99);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_SELL, '07-03-2019', 0, 58.21, 2, 14, 1.99);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_SELL, '07-03-2019', 0, 32.65, 2, 11, 1.99);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_PURCHASE, '07-03-2019', 0, 15.482, 14, 18, 1.99);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_PURCHASE, '07-03-2019', 0, 73.91, 2, 23, 1.99);
        $this->loadPaymentWithdrawal($manager, $this->portefolios[1], Transaction::CATEGORY_PAYMENT, '04-04-2019', 0, 200);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_PURCHASE, '04-04-2019', 0, 4.2291, 50, 19, 1.99);
        $this->loadPaymentWithdrawal($manager, $this->portefolios[1], Transaction::CATEGORY_PAYMENT, '29-04-2019', 0, 210);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_PURCHASE, '30-04-2019', 0, 200.29, 1, 20, 1.99);
        $this->loadPaymentWithdrawal($manager, $this->portefolios[1], Transaction::CATEGORY_PAYMENT, '03-06-2019', 0, 30);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_SELL, '03-06-2019', 0, 3.974, 50, 19, 1.99);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_SELL, '03-06-2019', 0, 93.45, 2, 16, 1.99);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_SELL, '03-06-2019', 0, 73.01, 2, 23, 1.99);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_PURCHASE, '03-06-2019', 0, 267.9, 2, 21, 3.21);
        $this->loadPaymentWithdrawal($manager, $this->portefolios[1], Transaction::CATEGORY_PAYMENT, '04-06-2019', 0, 250);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_PURCHASE, '05-06-2019', 0, 272.4, 1, 21, 1.99);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_SELL, '10-06-2019', 0, 14.35, 14, 18, 1.99);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_SELL, '10-06-2019', 0, 188.2421, 1, 20, 1.99);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_SELL, '12-06-2019', 0, 10.306, 21, 17, 1.99);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_SELL, '12-06-2019', 0, 20.39, 8, 15, 1.99);
        $this->loadPaymentWithdrawal($manager, $this->portefolios[1], Transaction::CATEGORY_PAYMENT, '12-06-2019', 0, 85);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_PURCHASE, '13-06-2019', 0, 277.9976, 3, 21, 5);
        $this->loadPaymentWithdrawal($manager, $this->portefolios[1], Transaction::CATEGORY_PAYMENT, '27-06-2019', 0, 280);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_PURCHASE, '27-06-2019', 0, 280.2, 1, 21, 1.99);
        $this->loadPaymentWithdrawal($manager, $this->portefolios[1], Transaction::CATEGORY_PAYMENT, '01-08-2019', 0, 110);
        $this->loadStockDividend($manager, $this->portefolios[1], Transaction::CATEGORY_STOCKDIVIDEND, '01-08-2019', 0, 9.6, 1, 13, 6.66);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_PURCHASE, '01-08-2019', 0, 10.2, 10, 13, 1.99);
        $this->loadPaymentWithdrawal($manager, $this->portefolios[1], Transaction::CATEGORY_PAYMENT, '05-08-2019', 0, 280);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_PURCHASE, '05-08-2019', 0, 279.3315, 1, 21, 1.99);
        $this->loadPaymentWithdrawal($manager, $this->portefolios[1], Transaction::CATEGORY_PAYMENT, '30-09-2019', 0, 300);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_PURCHASE, '30-09-2019', 0, 293.5, 1, 21, 1.99);
        $this->loadPaymentWithdrawal($manager, $this->portefolios[1], Transaction::CATEGORY_PAYMENT, '02-10-2019', 0, 280);
        $this->loadPaymentWithdrawal($manager, $this->portefolios[1], Transaction::CATEGORY_PAYMENT, '10-10-2019', 0, 10);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_PURCHASE, '10-10-2019', 0, 96, 3, 0, 1.99, 0.86, Transaction::FEE_TTF);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_DIVIDEND, '05-11-2019', 0, 0.79, 3, 0);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_SELL, '16-12-2019', 0, 13.6, 27, 13, 1.99);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_SELL, '20-01-2020', 0, 101.95, 3, 0, 1.99);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_SELL, '21-01-2020', 0, 320.5, 2, 21, 3.85);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_PURCHASE, '21-01-2020', 0, 48.03, 10, 5, 1.99, 1.44, Transaction::FEE_TTF);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_PURCHASE, '21-01-2020', 0, 129.1, 3, 1, 1.99, 1.16, Transaction::FEE_TTF);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_PURCHASE, '21-01-2020', 0, 404, 1, 4, 1.99, 1.21, Transaction::FEE_TTF);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_SELL, '05-02-2020', 0, 320, 1, 21, 1.99);
        $this->loadPaymentWithdrawal($manager, $this->portefolios[1], Transaction::CATEGORY_PAYMENT, '26-02-2020', 0, 160);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_PURCHASE, '26-02-2020', 0, 250, 2, 3, 1.99, 1.5, Transaction::FEE_TTF);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_SELL, '17-03-2020', 0, 229.2489, 2, 21, 1.99);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_SELL, '17-03-2020', 0, 229, 2, 21, 1.99);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_SELL, '17-03-2020', 0, 229, 2, 21, 1.99);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_PURCHASE, '17-03-2020', 0, 99, 5, 1, 1.99, 1.49, Transaction::FEE_TTF);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_PURCHASE, '18-03-2020', 0, 57, 8, 0, 1.99, 1.37, Transaction::FEE_TTF);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_PURCHASE, '19-03-2020', 0, 13.3, 31, 2, 1.99, 1.24, Transaction::FEE_TTF);
        $this->loadPurchaseSellDividend($manager, $this->portefolios[1], Transaction::CATEGORY_DIVIDEND, '01-04-2020', 0, 0.68, 10, 5);
        
        // Portefolios part
        PortefolioFactory::updateHistData($this->portefolios[0], new Transaction($this->logger), $this->logger, new DateTime()); // Actualise portefolio
        PortefolioFactory::updateHistData($this->portefolios[1], new Transaction($this->logger), $this->logger, new DateTime()); // Actualise portefolio

        foreach ($this->portefolios[0]->getLinesPf() as $line) {
            $manager->persist($line);
        }
        foreach ($this->portefolios[1]->getLinesPf() as $line) {
            $manager->persist($line);
        }
        foreach($this->portefolios[0]->getHistDataPortefolios() as $hist) {
            $manager->persist($hist);
        }
        foreach($this->portefolios[1]->getHistDataPortefolios() as $hist) {
            $manager->persist($hist);
        }
        $manager->persist($this->portefolios[0]);
        $manager->persist($this->portefolios[1]);
        
        $manager->flush();

        $this->logger->debug('====>> Loading all transactions in '.(microtime(true) - $start).' sec.');
    }
    
    /**
     * Load a payment or a withdrawal or general fee into database
     *
     * @param ObjectManager $manager
     * @param string $category Payment or Withdrawal
     * @param string $createAt format : d-m-Y, example : 28-01-1991
     * @param int $fs index of financial service array
     * @param float $amount
     * @param int $user index of user array, default = 0
     * @param string $description
     * @return void
     */
    private function loadPaymentWithdrawal(ObjectManager $manager, Portefolio $portefolio, string $category, string $createAt, 
        int $fs, float $amount, string $description = "", int $user = 0)
    {
        $tr = new Transaction($this->logger);
        $tr
            ->setCategory($category)
            ->setCreateAt(DateTime::createFromFormat('d-m-Y', $createAt))
            ->setAmount($amount)
            ->setFinancialService($this->financialServices[$fs])
            ->setUser($this->users[$user]);
        
        if (!empty($description)) $tr->setDescription($description);

        $manager->persist($tr);
        array_push($this->transactions, $tr);
        PortefolioFactory::addTransactionToPortefolio($manager, $portefolio, $tr, $this->logger);

        $this->logger->debug('Loading transaction '.$tr->getCategory().' on '.$tr->getCreateAt()->format('Y-m-d').'.');
    }
    
    /**
     * Load a purchase or a sell or a dividend into database
     *
     * @param ObjectManager $manager
     * @param string $category Purchase or Sell
     * @param string $createAt format : d-m-Y, example : 28-01-1991
     * @param integer $fs index of financial service array
     * @param float $price
     * @param integer $quantity
     * @param integer $stock
     * @param float $brokerage
     * @param float $fee
     * @param string $feeCategory
     * @param float $exchange
     * @param string $description
     * @param integer $user index of user array, default = 0
     * @return void
     */
    private function loadPurchaseSellDividend(ObjectManager $manager, Portefolio $portefolio, string $category, string $createAt,
    int $fs, float $price, int $quantity, int $stock, float $brokerage = 0.0, float $fee = 0.0, 
    string $feeCategory = "", float $exchange = 0.0, string $description = "", int $user = 0)
    {
        $tr = new Transaction($this->logger);
        $tr->setCategory($category)
            ->setCreateAt(DateTime::createFromFormat('d-m-Y', $createAt))
            ->setFinancialService($this->financialServices[$fs])
            ->setUser($this->users[$user])
            ->setStock($this->stocks[$stock])
            ->setPrice($price)
            ->setQuantity($quantity);
        
        if (!empty($brokerage)) $tr->setBrokerage($brokerage);
        if (!empty($fee)) $tr->setFee($fee);
        if (!empty($feeCategory)) $tr->setFeeCategory($feeCategory);
        if (!empty($exchange)) $tr->setExchange($exchange);
        if (!empty($description)) $tr->setDescription($description);
        
        $manager->persist($tr);
        array_push($this->transactions, $tr);
        PortefolioFactory::addTransactionToPortefolio($manager, $portefolio, $tr, $this->logger);

        $this->logger->debug('Loading transaction '.$tr->getCategory().' on '.$tr->getCreateAt()->format('Y-m-d').'.');
    }

    private function loadStockDividend(ObjectManager $manager, Portefolio $portefolio, string $category, string $createAt, int $fs, float $price, 
        int $quantity, int $stock, float $addon, string $description = "", int $user = 0)
    {
        $tr = new Transaction($this->logger);
        $tr
            ->setCategory($category)
            ->setCreateAt(DateTime::createFromFormat('d-m-Y', $createAt))
            ->setFinancialService($this->financialServices[$fs])
            ->setUser($this->users[$user])
            ->setStock($this->stocks[$stock])
            ->setPrice($price)
            ->setQuantity($quantity)
            ->setAddon($addon);

        $manager->persist($tr);
        array_push($this->transactions, $tr);
        PortefolioFactory::addTransactionToPortefolio($manager, $portefolio, $tr, $this->logger);

        $this->logger->debug('Loading transaction '.$tr->getCategory().' on '.$tr->getCreateAt()->format('Y-m-d').'.');
    }

    /**
     * Load user into database
     *
     * @param ObjectManager $manager
     * @return void
     */
    public function loadUsers(ObjectManager $manager)
    {
        $start = microtime(true);
        $user = new User();
        
        $user->setEmail('ava-adams@rambo.fr');
        $user->setLocale('fr');
        $user->setPassword($this->passwordEncoder->encodePassword($user, 'pass'));
        // $user->setPicture('https://img2.3pornstarmovies.com/contents/videos_screenshots/14000/14262/240x180/1.jpg');
        $user->setPicture('https://myteenwebcam.com/fapp/gifs/6c5993ee38e2318587effb6996ec6e98.gif');
        $user->setFullname('Ava Adams');
        
        array_push($this->users, $user);
        
        $manager->persist($user);
        
        $manager->flush();
        
        $this->logger->debug('Loading '.$user->getFullName().'.');
        $this->logger->debug('====>> Loading all users in '.(microtime(true) - $start).' sec.');
    }
}

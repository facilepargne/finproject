<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200408120506 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE transaction ADD debit DOUBLE PRECISION DEFAULT NULL, ADD credit DOUBLE PRECISION DEFAULT NULL, ADD amount DOUBLE PRECISION DEFAULT NULL, ADD ref_trans INT DEFAULT NULL, ADD exchange DOUBLE PRECISION DEFAULT NULL, CHANGE stock_id stock_id INT DEFAULT NULL, CHANGE price price DOUBLE PRECISION DEFAULT NULL, CHANGE quantity quantity INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE transaction DROP debit, DROP credit, DROP amount, DROP ref_trans, DROP exchange, CHANGE stock_id stock_id INT NOT NULL, CHANGE price price DOUBLE PRECISION NOT NULL, CHANGE quantity quantity INT NOT NULL');
    }
}

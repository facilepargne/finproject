<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200324171512 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE establishment (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, genre VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE financial_service (id INT AUTO_INCREMENT NOT NULL, establishment_id INT NOT NULL, name VARCHAR(255) NOT NULL, genre VARCHAR(255) NOT NULL, INDEX IDX_B35ED8B88565851 (establishment_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE stock (id INT AUTO_INCREMENT NOT NULL, currency_id INT NOT NULL, name VARCHAR(255) NOT NULL, short_name VARCHAR(255) NOT NULL, isin_code VARCHAR(255) NOT NULL, INDEX IDX_4B36566038248176 (currency_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE transaction (id INT AUTO_INCREMENT NOT NULL, stock_id INT NOT NULL, financial_service_id INT NOT NULL, category VARCHAR(255) NOT NULL, create_at DATETIME NOT NULL, price DOUBLE PRECISION NOT NULL, quantity INT NOT NULL, description LONGTEXT DEFAULT NULL, INDEX IDX_723705D1DCD6110 (stock_id), INDEX IDX_723705D1683E59CF (financial_service_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE financial_service ADD CONSTRAINT FK_B35ED8B88565851 FOREIGN KEY (establishment_id) REFERENCES establishment (id)');
        $this->addSql('ALTER TABLE stock ADD CONSTRAINT FK_4B36566038248176 FOREIGN KEY (currency_id) REFERENCES currency (id)');
        $this->addSql('ALTER TABLE transaction ADD CONSTRAINT FK_723705D1DCD6110 FOREIGN KEY (stock_id) REFERENCES stock (id)');
        $this->addSql('ALTER TABLE transaction ADD CONSTRAINT FK_723705D1683E59CF FOREIGN KEY (financial_service_id) REFERENCES financial_service (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE financial_service DROP FOREIGN KEY FK_B35ED8B88565851');
        $this->addSql('ALTER TABLE transaction DROP FOREIGN KEY FK_723705D1683E59CF');
        $this->addSql('ALTER TABLE transaction DROP FOREIGN KEY FK_723705D1DCD6110');
        $this->addSql('DROP TABLE establishment');
        $this->addSql('DROP TABLE financial_service');
        $this->addSql('DROP TABLE stock');
        $this->addSql('DROP TABLE transaction');
    }
}

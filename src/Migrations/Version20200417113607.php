<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200417113607 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE hist_data_stock (id INT AUTO_INCREMENT NOT NULL, stock_id INT NOT NULL, create_at DATETIME NOT NULL, open DOUBLE PRECISION NOT NULL, high DOUBLE PRECISION NOT NULL, low DOUBLE PRECISION NOT NULL, close DOUBLE PRECISION NOT NULL, volume INT NOT NULL, INDEX IDX_55D88B40DCD6110 (stock_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE line_pf (id INT AUTO_INCREMENT NOT NULL, stock_id INT NOT NULL, portefolio_id INT NOT NULL, quantity INT NOT NULL, share_price DOUBLE PRECISION NOT NULL, INDEX IDX_3875BD76DCD6110 (stock_id), INDEX IDX_3875BD76DA75771E (portefolio_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE portefolio (id INT AUTO_INCREMENT NOT NULL, fs_id INT NOT NULL, user_id INT NOT NULL, cash DOUBLE PRECISION NOT NULL, create_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_65BAA47C30DA66A0 (fs_id), INDEX IDX_65BAA47CA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE hist_data_stock ADD CONSTRAINT FK_55D88B40DCD6110 FOREIGN KEY (stock_id) REFERENCES stock (id)');
        $this->addSql('ALTER TABLE line_pf ADD CONSTRAINT FK_3875BD76DCD6110 FOREIGN KEY (stock_id) REFERENCES stock (id)');
        $this->addSql('ALTER TABLE line_pf ADD CONSTRAINT FK_3875BD76DA75771E FOREIGN KEY (portefolio_id) REFERENCES portefolio (id)');
        $this->addSql('ALTER TABLE portefolio ADD CONSTRAINT FK_65BAA47C30DA66A0 FOREIGN KEY (fs_id) REFERENCES financial_service (id)');
        $this->addSql('ALTER TABLE portefolio ADD CONSTRAINT FK_65BAA47CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE transaction ADD line_pf_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE transaction ADD CONSTRAINT FK_723705D1473984AC FOREIGN KEY (line_pf_id) REFERENCES line_pf (id)');
        $this->addSql('CREATE INDEX IDX_723705D1473984AC ON transaction (line_pf_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE transaction DROP FOREIGN KEY FK_723705D1473984AC');
        $this->addSql('ALTER TABLE line_pf DROP FOREIGN KEY FK_3875BD76DA75771E');
        $this->addSql('DROP TABLE hist_data_stock');
        $this->addSql('DROP TABLE line_pf');
        $this->addSql('DROP TABLE portefolio');
        $this->addSql('DROP INDEX IDX_723705D1473984AC ON transaction');
        $this->addSql('ALTER TABLE transaction DROP line_pf_id');
    }
}

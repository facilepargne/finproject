<?php

namespace App\Form;

use App\Entity\Stock;
use App\Entity\Transaction;
use App\Form\ApplicationType;
use App\Entity\FinancialService;
use Symfony\Component\Security\Core\Security;
use App\Repository\FinancialServiceRepository;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class TransactionMoneyType extends ApplicationType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category', ChoiceType::class, 
                $this->getConfigurationWithoutPlaceholder('Transaction.form.money.movement', 
                    [
                        'choices' => 
                        [
                            'Transaction.form.money.payment' => Transaction::CATEGORY_PAYMENT,
                            'Transaction.form.money.withdrawal' => Transaction::CATEGORY_WITHDRAWAL,
                        ],
                        'choice_translation_domain' => true,
                    ]))
            ->add('createAt', DateType::class, 
                $this->getConfigurationWithoutPlaceholder('Transaction.form.money.date', 
                    [
                        'widget' => 'single_text'
                    ]))
            ->add('financialService', EntityType::class, 
                $this->getConfiguration('Transaction.form.money.financialservice', 
                    'Transaction.form.money.placeholder.financialservice', 
                    [
                        'class' => FinancialService::class,
                        'query_builder' => function(FinancialServiceRepository $fsEr)
                            {
                                return $fsEr->createQueryBuilder('fs')
                                            ->andWhere('fs.user = :user')
                                            ->setParameter('user', $this->security->getUser())
                                            ->orderBy('fs.name');
                            },
                        'choice_label' => 'name',
                    ]))
            ->add('amount', NumberType::class, 
                $this->getConfiguration('Transaction.form.money.amount', 
                    'Transaction.form.money.placeholder.amount'))
            ->add('description', TextareaType::class, 
                $this->getConfiguration('Transaction.form.money.description', 
                    'Transaction.form.money.placeholder.description', 
                    [
                        'required' => false,
                    ]))
            ->add('save', SubmitType::class,
                $this->getConfigurationWithoutPlaceholder('Transaction.form.money.save'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Transaction::class,
        ]);
    }
}

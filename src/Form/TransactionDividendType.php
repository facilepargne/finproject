<?php

namespace App\Form;

use App\Entity\Stock;
use App\Entity\Transaction;
use App\Form\ApplicationType;
use App\Entity\FinancialService;
use Symfony\Component\Security\Core\Security;
use App\Repository\FinancialServiceRepository;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class TransactionDividendType extends ApplicationType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder 
        ->add('stock', EntityType::class, 
            $this->getConfigurationWithoutPlaceholder('Transaction.form.dividend.stock', 
                [
                    'class' => Stock::class,
                    'choice_label' => 'name',
                ]))
        ->add('category', ChoiceType::class, 
            $this->getConfigurationWithoutPlaceholder('Transaction.form.dividend.movement', 
                [
                    'choices' => 
                    [
                        'Transaction.form.dividend.dividend' => Transaction::CATEGORY_DIVIDEND,
                        'Transaction.form.dividend.stockdividend' => Transaction::CATEGORY_STOCKDIVIDEND,
                    ],
                    'choice_translation_domain' => true,
                ]))
        ->add('createAt', DateType::class, 
            $this->getConfigurationWithoutPlaceholder('Transaction.form.dividend.date', 
                [
                    'widget' => 'single_text'
                ]))
        ->add('financialService', EntityType::class, 
            $this->getConfiguration('Transaction.form.dividend.financialservice', 
                'Transaction.form.dividend.placeholder.financialservice', 
                [
                    'class' => FinancialService::class,
                    'query_builder' => function(FinancialServiceRepository $fsEr)
                        {
                            return $fsEr->createQueryBuilder('fs')
                                        ->andWhere('fs.user = :user')
                                        ->setParameter('user', $this->security->getUser())
                                        ->orderBy('fs.name');
                        },
                    'choice_label' => 'name',
                ]))
        ->add('price', NumberType::class, 
            $this->getConfiguration('Transaction.form.dividend.price', 
                'Transaction.form.dividend.placeholder.price'))
        ->add('quantity', IntegerType::class, 
            $this->getConfiguration('Transaction.form.dividend.quantity', 
                'Transaction.form.dividend.placeholder.quantity'))
        ->add('exchange', NumberType::class, 
            $this->getConfiguration('Transaction.form.dividend.exchange', 
                'Transaction.form.dividend.placeholder.exchange', 
                [
                    'required' => false,
                ]))
            ->add('addon', NumberType::class, 
                $this->getConfiguration('Transaction.form.dividend.addon', 
                    'Transaction.form.dividend.placeholder.addon',
                    [
                        'required' => false,
                    ]))        
        ->add('fee', NumberType::class, 
            $this->getConfiguration('Transaction.form.dividend.fee', 
                'Transaction.form.dividend.placeholder.fee',
                [
                    'required' => false,
                ]))
        ->add('feeCategory', ChoiceType::class, 
            $this->getConfiguration('Transaction.form.dividend.feecategory', 
                'Transaction.form.dividend.placeholder.feecategory', 
                [
                    'choices' => 
                    [
                        'Transaction.form.dividend.withholding' => Transaction::FEE_WITHHOLDING,
                    ],
                    'required' => false
                ]))
        ->add('description', TextareaType::class, 
            $this->getConfiguration('Transaction.form.dividend.description', 
                'Transaction.form.dividend.placeholder.description', 
                [
                    'required' => false,
                ]))
        ->add('save', SubmitType::class,
            $this->getConfigurationWithoutPlaceholder('Transaction.form.dividend.save'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Transaction::class,
        ]);
    }
}

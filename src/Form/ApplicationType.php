<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;

class ApplicationType extends AbstractType 
{
    /**
     * Permet d'avoir la configuration de base d'un champ !
     * 
     * @param string $label
     * @param string $placeholder
     * @param array $options
     * 
     * @return array
     */
    protected function getConfiguration($label, $placeholder, $options = [])
    { 
        return array_merge([
            'label' => $label,
            'attr' => [
                'placeholder' => $placeholder,
                ]
            ], $options);
    }

    /**
     * Get the base configuration without placeholder of a field 
     *
     * @param string $label
     * @param array $options
     * 
     * @return array
     */
    protected function getConfigurationWithoutPlaceholder($label, $options = [])
    {
        return array_merge([
            'label' => $label
        ], $options);
    }
}
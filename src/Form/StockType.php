<?php

namespace App\Form;

use App\Entity\Stock;
use App\Entity\Currency;
use App\Form\ApplicationType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;

class StockType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, 
                $this->getConfiguration('Configuration.form.stock.name', 
                    'Configuration.form.stock.placeholder.name'))
            ->add('shortName', TextType::class, 
                $this->getConfiguration('Configuration.form.stock.shortname', 
                    'Configuration.form.stock.placeholder.shortname'))
            ->add('isinCode', TextType::class, 
                $this->getConfiguration('Configuration.form.stock.isincode', 
                    'Configuration.form.stock.placeholder.isincode'))
            ->add('currency', EntityType::class, 
                $this->getConfiguration('Configuration.form.stock.currency', 
                    'Configuration.form.stock.placeholder.currency', 
                    [
                        'class' => Currency::class,
                        'choice_label' => 'name',
                    ]))
            ->add('sector', ChoiceType::class,
                $this->getConfiguration('Configuration.form.stock.sector', 
                    'Configuration.form.stock.placeholder.sector', 
                    [
                        'choices' => 
                        [
                            Stock::SECTOR_ENERGY => Stock::SECTOR_ENERGY, 
                            Stock::SECTOR_MATERIALS => Stock::SECTOR_MATERIALS, 
                            Stock::SECTOR_INDUSTRIALS => Stock::SECTOR_INDUSTRIALS, 
                            Stock::SECTOR_FINANCIALS => Stock::SECTOR_FINANCIALS, 
                            Stock::SECTOR_CONSUMER_DISCRETIONARY => Stock::SECTOR_CONSUMER_DISCRETIONARY, 
                            Stock::SECTOR_TELECOM => Stock::SECTOR_TELECOM, 
                            Stock::SECTOR_REAL_ESTATE => Stock::SECTOR_REAL_ESTATE, 
                            Stock::SECTOR_CONSUMER_STAPLES => Stock::SECTOR_CONSUMER_STAPLES, 
                            Stock::SECTOR_UTILITIES => Stock::SECTOR_UTILITIES, 
                            Stock::SECTOR_TECHNOLOGY => Stock::SECTOR_TECHNOLOGY, 
                            Stock::SECTOR_HEALTH_CARE => Stock::SECTOR_HEALTH_CARE, 
                            Stock::SECTOR_ETF => Stock::SECTOR_ETF, 
                        ],
                        'choice_translation_domain' => true,
                    ]))
            ->add('country', CountryType::class, 
                $this->getConfiguration('Configuration.form.stock.country', 
                'Configuration.form.stock.placeholder.country'))
            ->add('createAt', DateType::class, $this->getConfiguration('Configuration.form.stock.createat',
                'Configuration.form.stock.placeholder.createat', ['mapped' => false, 'widget' => 'single_text']))
            ->add('save', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Stock::class,
        ]);
    }
}

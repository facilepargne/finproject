<?php

namespace App\Form;

use App\Entity\Establishment;
use App\Form\ApplicationType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class EstablishmentType extends ApplicationType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, 
                $this->getConfiguration('Configuration.form.establishment.name',
                    'Configuration.form.establishment.placeholder.name'))
            ->add('genre', ChoiceType::class, 
                $this->getConfiguration('Configuration.form.establishment.genre', 
                    'Configuration.form.establishment.placeholder.genre', 
                    [
                        'choices' => 
                        [
                            'Configuration.form.establishment.bank' => 'bank', 
                            'Configuration.form.establishment.broker' => 'broker'
                        ],
                        'choice_translation_domain' => true,
                    ]))
            ->add('save', SubmitType::class,
                $this->getConfigurationWithoutPlaceholder('Configuration.form.establishment.save'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Establishment::class,
        ]);
    }
}

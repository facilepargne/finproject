<?php

namespace App\Form;

use App\Entity\Currency;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CurrencyType;

class CurrencyConfigurationType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, $this->getConfiguration('Configuration.form.currency.name',
                'Configuration.form.currency.placeholder.name'))
            ->add('shortName', CurrencyType::class, $this->getConfiguration('Configuration.form.currency.shortname',
                'Configuration.form.currency.placeholder.shortname'))
            ->add('abbreviation', TextType::class, $this->getConfiguration('Configuration.form.currency.abbreviation',
                'Configuration.form.currency.placeholder.abbreviation'))
            ->add('createAt', DateType::class, $this->getConfiguration('Configuration.form.currency.createat',
                'Configuration.form.currency.placeholder.createat', ['mapped' => false, 'widget' => 'single_text']))
            ->add('save', SubmitType::class,
                $this->getConfigurationWithoutPlaceholder('Configuration.form.currency.save'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Currency::class,
        ]);
    }
}

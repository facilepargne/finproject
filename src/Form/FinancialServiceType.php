<?php

namespace App\Form;

use App\Entity\Establishment;
use App\Form\ApplicationType;
use App\Entity\FinancialService;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class FinancialServiceType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, 
                $this->getConfiguration('Configuration.form.fs.name', 
                    'Configuration.form.fs.placeholder.name'))
            ->add('genre', ChoiceType::class,
                $this->getConfiguration('Configuration.form.fs.genre', 
                    'Configuration.form.fs.placeholder.genre', 
                    [
                        'choices' => 
                        [
                            'Configuration.form.fs.cto' => 'cto', 
                            'Configuration.form.fs.pea' => 'pea',
                            'Configuration.form.fs.peapme' => 'peapme',
                            'Configuration.form.fs.lifeinsurance' => 'lifeinsurance',
                        ],
                        'choice_translation_domain' => true,
                    ]))
            ->add('createAt', DateType::class, 
                $this->getConfigurationWithoutPlaceholder('Configuration.form.fs.date',
                    [
                        'widget' => 'single_text',
                    ]))
            ->add('balance', NumberType::class, 
                $this->getConfiguration('Configuration.form.fs.balance',
                    'Configuration.form.fs.placeholder.balance'))
            ->add('establishment', EntityType::class, 
                $this->getConfiguration('Configuration.form.fs.establishment', 
                    'Configuration.form.fs.placeholder.establishment',
                    [
                        'class' => Establishment::class,
                        'choice_label' => 'name',
                    ]))
            ->add('save', SubmitType::class,
                $this->getConfigurationWithoutPlaceholder('Configuration.form.fs.save'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FinancialService::class,
        ]);
    }
}

<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Stock;
use App\Form\StockType;
use App\Entity\Transaction;
use App\Entity\FinancialService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Security\Core\Security;
use App\Repository\FinancialServiceRepository;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class TransactionStockType extends ApplicationType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('stock', EntityType::class, 
                $this->getConfigurationWithoutPlaceholder('Transaction.form.stock.stock', 
                    [
                        'class' => Stock::class,
                        'choice_label' => 'name',
                    ]))
            ->add('category', ChoiceType::class, 
                $this->getConfigurationWithoutPlaceholder('Transaction.form.stock.movement', 
                    [
                        'choices' => 
                        [
                            'Transaction.form.stock.purchase' => Transaction::CATEGORY_PURCHASE,
                            'Transaction.form.stock.sell' => Transaction::CATEGORY_SELL,
                        ],
                        'choice_translation_domain' => true,
                    ]))
            ->add('createAt', DateType::class, 
                $this->getConfigurationWithoutPlaceholder('Transaction.form.stock.date', 
                    [
                        'widget' => 'single_text'
                    ]))
            ->add('financialService', EntityType::class, 
                $this->getConfiguration('Transaction.form.stock.financialservice', 
                    'Transaction.form.stock.placeholder.financialservice', 
                    [
                        'class' => FinancialService::class,
                        'query_builder' => function(FinancialServiceRepository $fsEr)
                            {
                                return $fsEr->createQueryBuilder('fs')
                                            ->andWhere('fs.user = :user')
                                            ->setParameter('user', $this->security->getUser())
                                            ->orderBy('fs.name');
                            },
                        'choice_label' => 'name',
                    ]))
            ->add('price', NumberType::class, 
                $this->getConfiguration('Transaction.form.stock.price', 
                    'Transaction.form.stock.placeholder.price'))
            ->add('quantity', IntegerType::class, 
                $this->getConfiguration('Transaction.form.stock.quantity', 
                    'Transaction.form.stock.placeholder.quantity'))
            ->add('exchange', NumberType::class, 
                $this->getConfiguration('Transaction.form.stock.exchange', 
                    'Transaction.form.stock.placeholder.exchange', 
                    [
                        'required' => false,
                    ]))
            ->add('brokerage', NumberType::class, 
                $this->getConfiguration('Transaction.form.stock.brokerage', 
                    'Transaction.form.stock.placeholder.brokerage'))
            ->add('fee', NumberType::class, 
                $this->getConfiguration('Transaction.form.stock.fee', 
                    'Transaction.form.stock.placeholder.fee',
                    [
                        'required' => false,
                    ]))
            ->add('feeCategory', ChoiceType::class, 
                $this->getConfiguration('Transaction.form.stock.feecategory', 
                    'Transaction.form.stock.placeholder.feecategory', 
                    [
                        'choices' => 
                        [
                            'Transaction.form.stock.ttf' => 'ttf',
                            'Transaction.form.stock.stampduty' => 'stampduty',
                        ],
                        'required' => false
                    ]))
            ->add('description', TextareaType::class, 
                $this->getConfiguration('Transaction.form.stock.description', 
                    'Transaction.form.stock.placeholder.description', 
                    [
                        'required' => false,
                    ]))
            ->add('save', SubmitType::class,
                $this->getConfigurationWithoutPlaceholder('Transaction.form.stock.save'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Transaction::class,
        ]);
    }
}

<?php

namespace App\Repository;

use App\Entity\LinePf;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method LinePf|null find($id, $lockMode = null, $lockVersion = null)
 * @method LinePf|null findOneBy(array $criteria, array $orderBy = null)
 * @method LinePf[]    findAll()
 * @method LinePf[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LinePfRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LinePf::class);
    }

    // /**
    //  * @return LinePf[] Returns an array of LinePf objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LinePf
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

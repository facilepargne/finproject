<?php

namespace App\Repository;

use App\Entity\HistDataCurrency;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method HistDataCurrency|null find($id, $lockMode = null, $lockVersion = null)
 * @method HistDataCurrency|null findOneBy(array $criteria, array $orderBy = null)
 * @method HistDataCurrency[]    findAll()
 * @method HistDataCurrency[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HistDataCurrencyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HistDataCurrency::class);
    }

    // /**
    //  * @return HistDataCurrency[] Returns an array of HistDataCurrency objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HistDataCurrency
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

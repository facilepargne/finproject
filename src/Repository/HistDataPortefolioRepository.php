<?php

namespace App\Repository;

use App\Entity\HistDataPortefolio;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method HistDataPortefolio|null find($id, $lockMode = null, $lockVersion = null)
 * @method HistDataPortefolio|null findOneBy(array $criteria, array $orderBy = null)
 * @method HistDataPortefolio[]    findAll()
 * @method HistDataPortefolio[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HistDataPortefolioRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HistDataPortefolio::class);
    }

    // /**
    //  * @return HistDataPortefolio[] Returns an array of HistDataPortefolio objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HistDataPortefolio
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

<?php

namespace App\Repository;

use App\Entity\HistDataStock;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method HistDataStock|null find($id, $lockMode = null, $lockVersion = null)
 * @method HistDataStock|null findOneBy(array $criteria, array $orderBy = null)
 * @method HistDataStock[]    findAll()
 * @method HistDataStock[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HistDataStockRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HistDataStock::class);
    }

    // /**
    //  * @return HistDataStock[] Returns an array of HistDataStock objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    
    public function findLastValue(): ?HistDataStock
    {
        return $this->createQueryBuilder('h')
            ->orderBy('h.createAt', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}

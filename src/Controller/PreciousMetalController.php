<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PreciousMetalController extends AbstractController
{
    /**
     * @Route("/{_locale}/precious/metal", name="precious_metal", requirements={"_locale"="%fe.locales%"})
     */
    public function index()
    {
        return $this->render('precious_metal/index.html.twig', [
            'controller_name' => 'PreciousMetalController',
        ]);
    }
}

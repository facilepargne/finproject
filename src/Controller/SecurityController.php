<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/{_locale}/login", name="app_login", requirements={"_locale"="%fe.locales%"})
     * @Route("/{_locale}/register", name="account_register", requirements={"_locale"="%fe.locales%"})
     * @Route("/{_locale}/account/update-password", name="account_password", requirements={"_locale"="%fe.locales%"})
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('homepage');
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/{_locale}/account", name="account_index", requirements={"_locale"="%fe.locales%"})
     */ 
    public function account()
    {
        return $this->render('security/account.html.twig');
    }

    /**
     * @Route("/{_locale}/logout", name="app_logout", requirements={"_locale"="%fe.locales%"})
     */
    public function logout()
    {
        return $this->redirectToRoute('homepage');
    }
}

<?php

namespace App\Controller;

use DateTime;
use App\Entity\Stock;
use App\Form\StockType;
use App\Entity\Currency;
use App\Entity\Portefolio;
use Psr\Log\LoggerInterface;
use App\Entity\Establishment;
use App\Form\EstablishmentType;
use App\Entity\FinancialService;
use App\Form\FinancialServiceType;
use App\Form\CurrencyConfigurationType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ConfigurationController extends AbstractController
{
    /**
     * @Route("/{_locale}/configuration", name="configuration", requirements={"_locale"="%fe.locales%"})
     * @param Request $request
     */
    public function index(Request $request, LoggerInterface $logger)
    {
        // Add stock form
        $responseStockForm = $this->loadStockForm($request, $logger);
        if ($responseStockForm instanceof RedirectResponse)
        {
            return $responseStockForm;
        }

        // Add financial service form
        $responseFSForm = $this->loadFSForm($request, $logger);
        if ($responseFSForm instanceof RedirectResponse)
        {
            return $responseFSForm;
        }

        // Add establishment form
        $responseEstablishmentForm = $this->loadEstablishmentForm($request, $logger);
        if ($responseEstablishmentForm instanceof RedirectResponse)
        {
            return $responseEstablishmentForm;
        }
        
        // Add currency form
        $responseCurrencyForm = $this->loadCurrencyForm($request, $logger);
        if ($responseCurrencyForm instanceof RedirectResponse)
        {
            return $responseCurrencyForm;
        }

        return $this->render('configuration/index.html.twig', [
            'formaddstock'          => $responseStockForm->createView(),
            'formaddfs'             => $responseFSForm->createView(),
            'formaddestablishment'  => $responseEstablishmentForm->createView(),
            'formaddcurrency'       => $responseCurrencyForm->createView(),
        ]);
    }

    /*
     * ================= Private functions ===========================
     */
    
    /**
     * Load a form to add stock
     *
     * @param Request $request
     * @return void
     */
    private function loadStockForm(Request $request, LoggerInterface $logger)
    {
        $stock = new Stock($logger);
        $form = $this->createForm(StockType::class, $stock);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $task = $form->getData();
            $stock->setLoadSince($form->get('createAt')->getData());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($stock);

            foreach ($stock->getHistDataStocks() as $key => $value) {
                $value->setStock($stock);
                $entityManager->persist($value);
            }
            
            $entityManager->flush();

            return $this->redirectToRoute('configuration');
        }

        return $form;
    }

    /**
     * Load a form to add financial service
     *
     * @param Request $request
     * @return void
     */
    private function loadFSForm(Request $request, LoggerInterface $logger)
    {
        $fs = new FinancialService($logger);
        $form = $this->createForm(FinancialServiceType::class, $fs);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $task = $form->getData();

            $user = $this->getUser();
            $user->addFss($fs);
            
            $portefolio = new Portefolio(DateTime::createFromFormat('Y-m-d', $fs->getCreateAt()->format('Y-m-d'))->setTime(0, 0), $logger);
            $portefolio->setFs($fs)
                        ->setUser($user);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($fs);
            $entityManager->persist($user);
            $entityManager->persist($portefolio);
            $entityManager->flush();

            return $this->redirectToRoute('configuration');
        }

        return $form;
    }

    /**
     * Load a form to add establishment
     *
     * @param Request $request
     * @return void
     */
    private function loadEstablishmentForm(Request $request, LoggerInterface $logger)
    {
        $es = new Establishment($logger);
        $form = $this->createForm(EstablishmentType::class, $es);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $task = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($es);
            $entityManager->flush();

            return $this->redirectToRoute('configuration');
        }

        return $form;
    }

    /**
     * Load a form to add currency
     *
     * @param Request $request
     * @return void
     */
    private function loadCurrencyForm(Request $request, LoggerInterface $logger)
    {
        $currency = new Currency($logger);
        $form = $this->createForm(CurrencyConfigurationType::class, $currency);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $task = $form->getData();
            $currency->setLoadSince($form->get('createAt')->getData());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($currency);

            foreach ($currency->getHistDataCurrencies() as $key => $value) {
                $value->setCurrency($currency);
                $entityManager->persist($value);
            }

            $entityManager->flush();

            return $this->redirectToRoute('configuration');
        }

        return $form;
    }
}

<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class RealEstateController extends AbstractController
{
    /**
     * @Route("/{_locale}/real/estate", name="real_estate", requirements={"_locale"="%fe.locales%"})
     */
    public function index()
    {
        return $this->render('real_estate/index.html.twig', [
            'controller_name' => 'RealEstateController',
        ]);
    }
}

<?php

namespace App\Controller;

use DateTime;
use App\Entity\User;
use App\Entity\Stock;
use App\Form\StockType;
use App\Entity\Portefolio;
use App\Entity\Transaction;
use Psr\Log\LoggerInterface;
use App\Entity\Establishment;
use App\Utils\PortefolioMaker;
use App\Form\EstablishmentType;
use App\Entity\FinancialService;
use App\Utils\PortefolioFactory;
use App\Form\TransactionMoneyType;
use App\Form\TransactionStockType;
use App\Repository\StockRepository;
use App\Utils\TradingDataExtractor;
use App\Form\TransactionDividendType;
use App\Repository\PortefolioRepository;
use App\Repository\TransactionRepository;
use App\Repository\EstablishmentRepository;
use App\Repository\HistDataStockRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\FinancialServiceRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Contracts\Translation\TranslatorInterface;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\GeoChart;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\PieChart;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\ColumnChart;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TradingController extends AbstractController
{
    /**
     * ======================= Portefolio part =======================
     */
    /**
     * Render one portefolio
     *
     * @Route("/{_locale}/trading/portefolio/{id}", name="trading_portefolio_id", requirements={"_locale"="%fe.locales%"})
     * 
     * @param PortefolioRepository $repoPf
     * @param LoggerInterface $logger
     * @return void
     */
    public function portefolio(Portefolio $pf, PortefolioRepository $repoPf, HistDataStockRepository $repoHist, StockRepository $repoStock, 
        LoggerInterface $logger, TranslatorInterface $translator)
    {
        $logger->info(__METHOD__.'Creation of a particular portefolio');

        $stockWorld = $repoStock->findOneBy(['shortName' => 'AMEW.F']);
        $stockCAC40 = $repoStock->findOneBy(['shortName' => 'C40.PA']);
        
        return $this->renderPortefolio([$pf], [$stockWorld, $stockCAC40], $logger, $translator);
    }
            
    /**
     * Render the portefolio page
     *
     * @Route("/{_locale}/trading/portefolio", name="trading_portefolio", requirements={"_locale"="%fe.locales%"})
     * 
     * @return Response
     */
    public function portefolios(PortefolioRepository $repoPf, HistDataStockRepository $repoHist, StockRepository $repoStock, 
        LoggerInterface $logger, TranslatorInterface $translator)
    {
        $logger->info('Loading global portefolio');
        
        $start = microtime(true);
        $arrayPf = $repoPf->findBy(['user' => $this->getUser()]); // Loading all portefolios of user
        $logger->debug('Fetching '.count($arrayPf).' portefolios takes '.(microtime(true) - $start).' sec');

        if (count($arrayPf) == 1) // If there is one portefolio in array
        {
            $logger->debug('One portefolio only existing, redirect to portefolio (id: '.$arrayPf[0]->getId().').');
            return $this->redirectToRoute('trading_portefolio_id', ['id' => $arrayPf[0]->getId()]);
        }

        $stockWorld = $repoStock->findOneBy(['shortName' => 'AMEW.F']);
        $stockCAC40 = $repoStock->findOneBy(['shortName' => 'C40.PA']);
        
        return $this->renderPortefolio($arrayPf, [$stockWorld, $stockCAC40], $logger, $translator);
        
    }

    private function renderPortefolio($arrayPf, $arrayStockConst, $logger, $translator)
    {
        $arrayGlobal = null;
        if (count($arrayPf) > 1)
            $arrayGlobal = PortefolioFactory::getDataForPortefolios($arrayPf, new DateTime(), true, $logger);
        else
            PortefolioFactory::getDataForPortefolio($arrayPf[0], new DateTime(), true, $logger);

        $start = microtime(true);
        $sectorPieChart = TradingDataExtractor::getStockBySector($arrayPf, $translator, $logger);
        $logger->debug('Creating pie chart for portefolios takes '.(microtime(true) - $start).' sec');
        
        $start = microtime(true);
        $sectorGeoChart = TradingDataExtractor::getStockByGeo($arrayPf, $translator, $logger);
        $logger->debug('Creating geo chart for portefolios takes '.(microtime(true) - $start).' sec');
        
        $start = microtime(true);
        $dividendChart = TradingDataExtractor::getDividendsByYear($arrayPf, $translator, $logger);
        $logger->debug('Creating dividend bar chart for portefolios takes '.(microtime(true) - $start).' sec');
        
        $start = microtime(true);
        $valuationChart = TradingDataExtractor::getValuationLineChart($arrayPf, $translator, $logger);
        $logger->debug('Creating valuation line chart for portefolios takes '.(microtime(true) - $start).' sec');
        
        $start = microtime(true);
        $partChart = TradingDataExtractor::getPartPercentLineChart($arrayPf, $arrayStockConst, $translator);
        $logger->debug('Creating part line chart for portefolios takes '.(microtime(true) - $start).' sec');

        return $this->render('trading/portefolios.html.twig', [
            'portefolios'       => $arrayPf,
            'valuation'         => is_null($arrayGlobal) ? $arrayPf[0]->getValuation() : $arrayGlobal[0],
            'cash'              => is_null($arrayGlobal) ? $arrayPf[0]->getCash() : $arrayGlobal[1],
            'total'             => is_null($arrayGlobal) ? $arrayPf[0]->getValuation() + $arrayPf[0]->getCash() : $arrayGlobal[2],
            'payments'          => is_null($arrayGlobal) ? $arrayPf[0]->getPayments() : $arrayGlobal[3],
            'date'              => is_null($arrayGlobal) ? $arrayPf[0]->getLastValuation() : $arrayGlobal[4],
            'xirr'              => is_null($arrayGlobal) ? $arrayPf[0]->getXirr() : $arrayGlobal[5],
            'globalXirr'        => is_null($arrayGlobal) ? $arrayPf[0]->getGlobalXirr() : $arrayGlobal[6],
            'creationDate'      => is_null($arrayGlobal) ? $arrayPf[0]->getCreateAt() : $arrayGlobal[7],
            'sectorpiechart'    => $sectorPieChart,
            'geochart'          => $sectorGeoChart,
            'dividendchart'     => $dividendChart,
            'valuationchart'    => $valuationChart,
            'partchart'         => $partChart,
        ]);
    }
    /**
     * ======================= End Portefolio part =======================
     */

    /**
     * Render the transactions page
     *
     * @Route("/{_locale}/trading/transactions", name="trading_transactions", requirements={"_locale"="%fe.locales%"})
     * @param TransactionRepository $repo
     * @param Request $request
     * @return Response
     */
    public function transactions(TransactionRepository $repoTr, Request $request, LoggerInterface $logger)
    {
        $transactions = $repoTr->findBy(
            ['user' => $this->getUser()->getId()],
            [
                'createAt' => 'ASC',
                'id' => 'ASC'
            ]);

        // Get all financial services
        $arrayFs = [];
        foreach ($transactions as $tr) {
            $fs = $tr->getFinancialService();
            if (!in_array($fs, $arrayFs))
                array_push($arrayFs, $fs);
        }

        // Add purchase/sell transaction form
        $responseTransStock = $this->loadTransactionStockForm($request, $logger);
        if ($responseTransStock instanceof RedirectResponse)
        {
            return $responseTransStock;
        }

        // Add dividende transaction form
        $responseTransDividend = $this->loadTransactionDividendForm($request, $logger);
        if ($responseTransDividend instanceof RedirectResponse)
        {
            return $responseTransDividend;
        }

        // Add money transaction form
        $responseTransMoney = $this->loadTransactionMoneyForm($request, $logger);
        if ($responseTransMoney instanceof RedirectResponse)
        {
            return $responseTransMoney;
        }

        return $this->render('trading/transactions.html.twig', [
            'transactions'      => $transactions,
            'arrayFs'           => $arrayFs,
            'formtransstock'    => $responseTransStock->createView(),
            'formtransdividend' => $responseTransDividend->createView(),
            'formtransmoney'    => $responseTransMoney->createView(),
        ]);
    }

    /**
     * Render the transactions page
     *
     * @Route("/{_locale}/trading/transactions/{id}", name="trading_transactions_fs", requirements={"_locale"="%fe.locales%"})
     * @param TransactionRepository $repo
     * @param Request $request
     * @return Response
     */
    public function transactionsFs(FinancialService $fs, TransactionRepository $repoTr, Request $request, LoggerInterface $logger)
    {
        $transactions = $repoTr->findBy(
            [
                'user' => $this->getUser()->getId(),
                'financialService' => $fs
            ],
            [
                'createAt' => 'ASC',
                'id' => 'ASC'
            ]
        );

        // Add purchase/sell transaction form
        $responseTransStock = $this->loadTransactionStockForm($request, $logger);
        if ($responseTransStock instanceof RedirectResponse)
        {
            return $responseTransStock;
        }

        // Add dividende transaction form
        $responseTransDividend = $this->loadTransactionDividendForm($request, $logger);
        if ($responseTransDividend instanceof RedirectResponse)
        {
            return $responseTransDividend;
        }

        // Add money transaction form
        $responseTransMoney = $this->loadTransactionMoneyForm($request, $logger);
        if ($responseTransMoney instanceof RedirectResponse)
        {
            return $responseTransMoney;
        }

        return $this->render('trading/transactions.html.twig', [
            'transactions'      => $transactions,
            'fs'                => $fs,
            'formtransstock'    => $responseTransStock->createView(),
            'formtransdividend' => $responseTransDividend->createView(),
            'formtransmoney'    => $responseTransMoney->createView(),
        ]);
    }

    /*
     * ================= Private functions ===========================
     */
    
    /**
     * Load a form to add purchase/sell transaction 
     *
     * @param Request $request
     * @return void
     */
    private function loadTransactionStockForm(Request $request, LoggerInterface $logger)
    {
        $tr = new Transaction($logger);
        $form = $this->createForm(TransactionStockType::class, $tr);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $task = $form->getData();
            $user = $this->getUser();
            $tr->setUser($user);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tr);
            $this->persistPortefolio($user, $tr, true, $logger);
            
            $entityManager->flush();

            return $this->redirectToRoute('trading_transactions');
        }

        return $form;
    }

    /**
     * Load a form to add dividend transaction 
     *
     * @param Request $request
     * @return void
     */
    private function loadTransactionDividendForm(Request $request, LoggerInterface $logger)
    {
        $tr = new Transaction($logger);
        $form = $this->createForm(TransactionDividendType::class, $tr);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $task = $form->getData();
            $user = $this->getUser();
            $tr->setUser($user);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tr);
            $this->persistPortefolio($user, $tr, true, $logger);
            $entityManager->flush();

            return $this->redirectToRoute('trading_transactions');
        }

        return $form;
    }

    /**
     * Load a form to add money transaction 
     *
     * @param Request $request
     * @return void
     */
    private function loadTransactionMoneyForm(Request $request, LoggerInterface $logger)
    {
        $tr = new Transaction($logger);
        $form = $this->createForm(TransactionMoneyType::class, $tr);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $task = $form->getData();
            $user = $this->getUser();
            $tr->setUser($user);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tr);
            $entityManager->flush();

            $this->persistPortefolio($user, $tr, false, $logger);
            $entityManager->flush();

            return $this->redirectToRoute('trading_transactions');
        }

        return $form;
    }

    /**
     * Persist portefolio
     *
     * @param User $user
     * @param Transaction $tr
     * @return void
     */
    private function persistPortefolio(User $user, Transaction $tr, bool $updateLine, LoggerInterface $logger)
    {
        $portefolio = null;
        foreach ($user->getPortefolios() as $key => $value) { // We find the proper portefolio
            if ($value->getFs() == $tr->getFinancialService())
            {
                $portefolio = $value;
            }
        }
        PortefolioFactory::addTransactionToPortefolio($portefolio, $tr, $logger); // We update the portefolio

        $entityManager = $this->getDoctrine()->getManager();

        if ($updateLine)
        {
            foreach ($portefolio->getLinesPf() as $line) {
                $entityManager->persist($line);
            }
        }
        foreach($portefolio->getHistDataPortefolios() as $hist) {
            $entityManager->persist($hist);
        }
        $entityManager->persist($portefolio);
        $entityManager->flush();
    }

    /**
     * Generate data for portefolio
     * [$valuation, $cash, $total, $payments, $date, $xirr, $globalXirr, $startDate]
     *
     * @param [type] $arrayPf, Collection of portefolios
     * @return array 
     */
    private function getDataForPortefolio($arrayPf, LoggerInterface $logger)
    {
        $valuation = 0; // Valuation of assets
        $cash = 0; // Cash
        $total = 0; // Total of cash + valuation
        $payments = 0; // Total of payments
        $startDate = new DateTime();
        $lastDate = new DateTime();

        foreach ($arrayPf as $pf) {
            $pf = PortefolioFactory::updateSharePrice($pf);
            $pf = PortefolioFactory::updateAmount($pf);
            $pf = PortefolioFactory::updateUCP($pf);
            $pf = PortefolioFactory::updateLatentGl($pf, $logger);
            $valuation += PortefolioFactory::getValuation($pf);
            $cash += $pf->getCash(); 
            $payments += $pf->getPayments();
            if ($pf->getCreateAt() < $startDate) $startDate = $pf->getCreateAt();
        }
        $total = $valuation + $cash;

        $xirr = PortefolioFactory::getXirrData($arrayPf, $total);
        $globalXirr = PortefolioFactory::getGlobalXirrData($xirr, new DateTime('2018-04-27'), new DateTime);

        return [$valuation, $cash, $total, $payments, $lastDate, $xirr, $globalXirr, $startDate];
    }
}

<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class TaxController extends AbstractController
{
    /**
     * @Route("/{_locale}/tax", name="tax", requirements={"_locale"="%fe.locales%"})
     */
    public function index()
    {
        return $this->render('tax/index.html.twig', [
            'controller_name' => 'TaxController',
        ]);
    }
}

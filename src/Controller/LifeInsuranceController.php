<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class LifeInsuranceController extends AbstractController
{
    /**
     * @Route("/{_locale}/life/insurance", name="life_insurance", requirements={"_locale"="%fe.locales%"})
     */
    public function index()
    {
        return $this->render('life_insurance/index.html.twig', [
            'controller_name' => 'LifeInsuranceController',
        ]);
    }
}

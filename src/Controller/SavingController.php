<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SavingController extends AbstractController
{
    /**
     * @Route("/{_locale}/saving", name="saving", requirements={"_locale"="%fe.locales%"})
     */
    public function index()
    {
        return $this->render('saving/index.html.twig', [
            'controller_name' => 'SavingController',
        ]);
    }
}

<?php

namespace App\Utils;

use DateTime;
use App\Entity\Transaction;
use Psr\Log\LoggerInterface;
use App\Utils\Entity\Portefolio;
use Symfony\Contracts\Translation\TranslatorInterface;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\GeoChart;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\PieChart;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\LineChart;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\ColumnChart;

class TradingDataExtractor 
{
    /**
     * Load data from portefolios and get stock by sector. Format it to be given to a chart.
     *
     * @param [type] $portefolios
     * @return void
     */
    public static function getStockBySector($portefolios, TranslatorInterface $translator, LoggerInterface $logger)
    {
        $sectorArray = self::getAmountBy($portefolios, 'sector', $logger);
        $sum = array_sum($sectorArray);

        foreach($sectorArray as $key => $value)
        {
            $sectorArray[$key] /= $sum;
            $sectorArray[$key] *= 100;
        }

        $sectorArray = self::transformArray($sectorArray, $translator);

        return self::getDataBySector($sectorArray, $translator);
    }

    /**
     * Load data from portefolios and get stock by countries. Format it to be given to a chart.
     *
     * @param [type] $portefolios
     * @return void
     */
    public static function getStockByGeo($portefolios, TranslatorInterface $translator, LoggerInterface $logger)
    {
        $geoArray = self::getAmountBy($portefolios, 'country', $logger);

        return self::getDataByGeo(self::transformArray($geoArray), $translator);
    }

    /**
     * Load data from portefolios and get dividends by year. Format it to be given to a chart.
     *
     * @param [type] $portefolios
     * @return void
     */
    public static function getDividendsByYear($portefolios, TranslatorInterface $translator, LoggerInterface $logger)
    {
        $dividendArray = [];

        foreach($portefolios as $p)
        {
            foreach($p->getLinesPf() as $line)
            {
                foreach($line->getTransactions() as $tr)
                {
                    $grossDividend = 0;
                    $netDividend = 0;
                    $year = $tr->getCreateAt()->format('Y'); // Year
                    // Exchange part
                    $exchange = 1;
                    if (!is_null($tr->getExchange())) $exchange = $tr->getExchange();

                    if ($tr->getCategory() == Transaction::CATEGORY_DIVIDEND) // Fetch normal dividend
                    {
                        $grossDividend = round(($tr->getPrice() * $tr->getQuantity()) / $exchange, 2);
                        $netDividend = round($tr->getCredit(), 2);
                    } 
                    elseif ($tr->getCategory() == Transaction::CATEGORY_STOCKDIVIDEND) // Fetch stock dividend
                    {
                        $grossDividend = round(($tr->getPrice() * $tr->getQuantity()) - $tr->getAddon(), 2);
                        $netDividend = $grossDividend;
                    }

                    if ($grossDividend != 0 && $netDividend != 0)
                    {
                        if (array_key_exists($year, $dividendArray))
                        {
                            $dividendArray[$year][0] += $grossDividend;
                            $dividendArray[$year][1] += $netDividend;
                        }
                        else 
                        {
                            $dividendArray[$year] = [$grossDividend, $netDividend];
                        }
                    }
                }
            }
        }

        // Transform dividend array into a readable data for chart
        $tradingData = [];
        foreach($dividendArray as $key => $value)
        {
            array_push($tradingData, [strval($key), $value[0], $value[1]]);
        }

        $columnChart = new ColumnChart();

        $arrayTitle = 
        [
            $translator->trans('Portefolio.chart.dividend.year'), 
            $translator->trans('Portefolio.chart.dividend.gross'),
            $translator->trans('Portefolio.chart.dividend.net')
        ];
        array_unshift($tradingData, $arrayTitle);

        $columnChart->getData()->setArrayToDataTable($tradingData);

        $columnChart->getOptions()->getVAxis()->setTitle($translator->trans('Portefolio.chart.dividend.vaxis'));
        $columnChart->getOptions()->getLegend()->setPosition('bottom');

        return $columnChart;
    }

    public static function getPartPercentLineChart($arrayPf, $arrayStock, TranslatorInterface $translator)
    {
        $histDataArray = [];
        foreach($arrayPf as $p) 
        {
            foreach($p->getHistDataPortefolios() as $h)
            {
                $date = $h->getCreateAt()->format('Y-m-d');
                if ($date != date('Y-m-d'))
                {
                    if (array_key_exists($date, $histDataArray))
                    {
                        $histDataArray[$date][0] += $h->getPayments();
                        $histDataArray[$date][1] += $h->getValuation();
                    }
                    else
                    {
                        $histDataArray[$date][0] = $h->getPayments();
                        $histDataArray[$date][1] = $h->getValuation();
                    }
                }
            }
        }

        $histConst = [];
        foreach($arrayStock as $stock)
        {
            if (!is_null($stock))
                array_push($histConst, $stock->getHistDataStocks());
        }

        $part = 100;
        $nbPart = 0;
        $payment = 0;
        $const = [];
        for ($i=0; $i < count($histConst); $i++) { 
            $const[$i] = [100, 0];
        }
        $tradingData = [];
        foreach($histDataArray as $key => $h)
        {
            if ($nbPart == 0) // First payment
            {
                // Portefolio
                $nbPart = $h[0] / $part;
                $payment = $h[0];

                // Const
                for ($i=0; $i < count($histConst); $i++) {
                    $value = self::getValueFromDateInHistData($histConst[$i]->toArray(), DateTime::createFromFormat('Y-m-d', $key));
                    $const[$i][1] = $value / $const[$i][0];
                }
            }
            else
            {
                if ($h[0] > $payment)
                {
                    $diff = $h[0] - $payment;
                    $payment = $h[0];
                    $nbPart += ($diff / $part);
                }
                $part = $h[1] / $nbPart;

                // Const
                for ($i=0; $i < count($histConst); $i++) {
                    $value = self::getValueFromDateInHistData($histConst[$i]->toArray(), DateTime::createFromFormat('Y-m-d', $key));
                    $const[$i][0] = $value / $const[$i][1];
                }
            }
            
            $histData = [];
            array_push($histData, DateTime::createFromFormat('Y-m-d', $key));
            for ($i=0; $i < count($histConst); $i++) {
                array_push($histData, $const[$i][0]);
            }
            array_push($histData, $part);

            array_push($tradingData, $histData);
        }

        $lineChart = new LineChart();

        $arrayTitle = [];
        array_push($arrayTitle, $translator->trans('Portefolio.chart.part.date'));
        for ($i=0; $i < count($histConst); $i++) {
            array_push($arrayTitle, $arrayStock[$i]->getName());
        }
        array_push($arrayTitle, $translator->trans('Portefolio.chart.part.ljcorp'));

        array_unshift($tradingData, $arrayTitle);

        $lineChart->getData()->setArrayToDataTable($tradingData);

        $lineChart->getOptions()->getLegend()->setPosition('bottom');

        return $lineChart;
    }

    /**
     * Create a line chart with valuation of portefolios
     *
     * @param [type] $arrayPf
     * @return void
     */
    public static function getValuationLineChart($arrayPf, TranslatorInterface $translator, LoggerInterface $logger)
    {
        $histDataArray = [];
        foreach($arrayPf as $p) 
        {
            foreach($p->getHistDataPortefolios() as $key => $h)
            {
                $date = $h->getCreateAt()->format('Y-m-d');
                $logger->debug('Histdata : '.$date.' of payments('.$h->getPayments().') and valuation('.$h->getValuation().')');
                if ($date != date('Y-m-d'))
                {
                    if (array_key_exists($date, $histDataArray))
                    {
                        $histDataArray[$date][0] += $h->getPayments();
                        $histDataArray[$date][1] += $h->getValuation();
                    }
                    else
                    {
                        $histDataArray[$date][0] = $h->getPayments();
                        $histDataArray[$date][1] = $h->getValuation();
                    }
                }
            }
        }

        $tradingData = [];
        foreach($histDataArray as $key => $h)
        {
            array_push($tradingData, [DateTime::createFromFormat('Y-m-d', $key), $h[0], $h[1]]);
        }

        $lineChart = new LineChart();

        $arrayTitle = 
        [
            $translator->trans('Portefolio.chart.valuation.date'), 
            $translator->trans('Portefolio.chart.valuation.payments'),
            $translator->trans('Portefolio.chart.valuation.valuation'),
        ];
        array_unshift($tradingData, $arrayTitle);

        $lineChart->getData()->setArrayToDataTable($tradingData);

        $lineChart->getOptions()->getVAxis()->setTitle($translator->trans('Portefolio.chart.valuation.vaxis'));
        $lineChart->getOptions()->getLegend()->setPosition('bottom');

        return $lineChart;
    }

    public static function getValueFromDateInHistData($histData, DateTime $date)
    {
        if (is_array($histData) && $date > $histData[array_key_last($histData)]->getCreateAt())
        {
            return round($histData[array_key_last($histData)]->getAdjClose(), 2);
        }
        foreach($histData as $key => $h)
        {
            if ($h->getCreateAt()->setTime(0, 0) == $date->setTime(0, 0))
            {
                for ($i=$key; $i >= 0; $i--) { // If current value is 0 then get previous one and again and again
                    if ($histData[$i]->getAdjClose() != 0) {
                        return round($histData[$i]->getAdjClose(), 2);
                    }
                }
            } 
            elseif ($h->getCreateAt()->setTime(0, 0) > $date->setTime(0, 0))
            {
                if ($key != 0)
                {
                    for ($i=$key - 1; $i >= 0; $i--) { // If current value is 0 then get previous one and again and again
                        if ($histData[$i]->getAdjClose() != 0) {
                            return round($histData[$i]->getAdjClose(), 2);
                        }
                    }
                }
                else
                {
                    return round($h->getAdjClose(), 2);
                }
            } 
        }
        return 0; // Error
    }

    private static function transformArray($array, TranslatorInterface $translator = null)
    {
        $result = [];
        foreach($array as $key => $value)
        {
            if (is_null($translator))
                array_push($result, [$key, $value]);
            else
                array_push($result, [$translator->trans($key), $value]);
        }
        return $result;
    }

    /**
     * Get amount filtered by sector or country
     *
     * @param [type] $portefolio
     * @param [type] $filter
     * @return void
     */
    private static function getAmountBy($portefolios, $filter, $logger)
    {
        $stockArray = [];

        foreach($portefolios as $p)
        {
            foreach($p->getLinesPf() as $line)
            {
                if ($line->getStatus() == true)
                {
                    $stockAmount = $line->getAmount();
                    $stockFilter = 0;

                    switch ($filter)
                    {
                        case 'sector':
                            $stockFilter = $line->getStock()->getSector();
                        break;
                        case 'country':
                            $stockFilter = $line->getStock()->getCountry();
                        break;
                    }
                    
                    if (array_key_exists($stockFilter, $stockArray))
                    {
                        $stockArray[$stockFilter] += $stockAmount;
                    } 
                    else
                    {
                        $stockArray[$stockFilter] = $stockAmount;
                    }
                }
            }
        }

        return $stockArray;
    }

    /**
     * Create a PieChart Object with all data
     *
     * @param [type] $tradingData
     * @return PieChart
     */
    private static function getDataBySector($tradingData, TranslatorInterface $translator)
    {
        $pieChart = new PieChart();

        $arrayTitle = 
        [
            $translator->trans('Portefolio.chart.sector.sector'), 
            $translator->trans('Portefolio.chart.sector.percent')
        ];
        array_unshift($tradingData, $arrayTitle);

        $pieChart->getData()->setArrayToDataTable($tradingData);
        $pieChart->getOptions()->setHeight('100%');
        $pieChart->getOptions()->setWidth('100%');
        $pieChart->getOptions()->getChartArea()->setWidth('100%');
        $pieChart->getOptions()->getChartArea()->setHeight('100%');
        $pieChart->getOptions()->getChartArea()->setTop(10);
        $pieChart->getOptions()->setPieHole(0.4);

        return $pieChart;
    }

    /**
     * Create a GeoChart Object with all data
     *
     * @param [type] $tradingData
     * @return void
     */
    private static function getDataByGeo($tradingData, TranslatorInterface $translator)
    {
        $geoChart = new GeoChart();

        $arrayTitle = 
        [
            $translator->trans('Portefolio.chart.geo.country'), 
            $translator->trans('Portefolio.chart.geo.amount')
        ];
        array_unshift($tradingData, $arrayTitle);

        $geoChart->getData()->setArrayToDataTable($tradingData);

        return $geoChart;
    }
}
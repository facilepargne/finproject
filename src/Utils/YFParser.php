<?php

namespace App\Utils;

use DateTime;
use DateInterval;
use App\Entity\Stock;
use App\Entity\Currency;
use Symfony\Component\DomCrawler\Crawler;

class YFParser
{
    /**
     * Xpath filter for profile
     */
    private const XPATH_PROFILE = '//p[@class="D(ib) Va(t)"]/span';

    /**
     * Xpath filter for current share price
     */
    private const XPATH_CURRENT_SHARE_PRICE = '//span[@class="Trsdu(0.3s) Fw(b) Fz(36px) Mb(-4px) D(ib)"]';

    /**
     * Xpath filter for historical data
     */
    private const XPATH_HISTORICAL_DATA = '//table[@data-test="historical-prices"]/tbody/tr/td/span';

    /**
     * Base URL from Yahoo Finance website
     */
    private const YAHOO_FINANCE_BASE_URL = 'https://finance.yahoo.com/';

    /**
     * Quote for Yahoo Finane website
     */
    private const YAHOO_FINANCE_QUOTE = 'quote/';

    /**
     * Profile for Yahoo Finance website
     */
    private const YAHOO_FINANCE_PROFILE = '/profile?p=';

    /**
     * Part of historical data for Yahoo FInance website
     */
    const YAHOO_FINANCE_BASE = 'https://query1.finance.yahoo.com/v7/finance/download/';
    const YAHOO_FINANCE_HISTORY = '/history';
    const YAHOO_FINANCE_PERIOD1 = '?period1=';
    const YAHOO_FINANCE_PERIOD2 = '&period2=';
    const YAHOO_FINANCE_END_PART = '&interval=1d&filter=history&frequency=1d';
    const YAHOO_FINANCE_END_PART_DL = '&interval=1d&events=history';

    /**
     * Get Profile form a stock
     *
     * @param Stock $stock
     * @return array, contains sector and activity sector
     */
    public static function getProfileFromStock(Stock $stock)
    {
        // echo 'Debug caller : '.debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS,2)[1]['function'].PHP_EOL;
        $url = self::createProfileLinkFromStock($stock->getShortName());
        $html = self::getRawHTML($url);
        return self::parseProfileFromHTML($html);
    }

    public static function getHistDataFromStock(Stock $stock, DateTime $beginDate, DateTime $endDate)
    {
        $url = self::createHistDataDownloadLinkFromStock($stock->getShortName(), $beginDate, $endDate);
        return self::parseHistoricalDataFromHTML($url);
    }

    public static function getHistDataFromCurrency(Currency $currency, DateTime $beginDate, DateTime $endDate)
    {
        $url = self::createHistDataDownloadLinkFromCurrency($currency->getShortName(), $beginDate, $endDate);
        return self::parseHistoricalDataFromHTMLCurrency($url);
    }

    /**
     * Get raw HTML result from URL with cURL
     *
     * @param string $url
     * @return string raw HTML
     */
    public static function getRawHTML(string $url)
    {
        // echo 'Debug caller : '.debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS,2)[1]['function'].PHP_EOL;
        $ch=curl_init(); // Initiate a cURL session
        // Emulate web browser
        $agent = 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0';
        // Avoid SSL checker
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        //curl_setopt($ch, CURLOPT_VERBOSE, true);
        // Get raw html in result
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Follow link if result is a redirection
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_USERAGENT, $agent);
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        $result=curl_exec($ch);
        if(curl_errno($ch)){
            echo 'Curl error: ' . curl_error($ch);
        }
        curl_close($ch); // Close cURL session

        return $result;
    }

    /**
     * Return an array of sector and activity sector
     *
     * @param string $html, raw data of the html page
     * @return array, array of sector and activity sector
     */
    public static function parseProfileFromHTML(string $html)
    {
        $crawler = self::getCrawlerWithFilter($html, self::XPATH_PROFILE);

        // Create array of profile
        return [
            Stock::SECTOR => $crawler->count() >= 2 ? $crawler->eq(1)->text() : null,
            Stock::ACTIVITY_SECTOR => $crawler->count() >= 4 ? $crawler->eq(3)->text() : null,
        ];
    }

    /**
     * Return the share price
     *
     * @param string $html, raw data of the html page
     * @return array, array of sector and activity sector
     */
    public static function parseSharePriceFromHTML(string $html)
    {
        $crawler = self::getCrawlerWithFilter($html, self::XPATH_CURRENT_SHARE_PRICE);

        return $crawler->count() > 0 ? $crawler->eq(0)->text() : 0;
    }

    /**
     * Return an array of historical data of the stock quote : 
     * - Date
     * - Open
     * - High
     * - Low
     * - Close (Close price adjusted for splits)
     * - Adj Close (Adjusted close price adjusted for both dividends and splits)
     * - Volume
     *
     * @param string $url, raw data of the html page
     * @return array, array of historical data of the stock quote
     */
    public static function parseHistoricalDataFromHTML(string $url)
    {
        $firstLine = true;
        $histData = array();
        if (($handle = fopen($url, "r")) !== FALSE) 
        {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) 
            {
                if ($firstLine)
                {
                    $firstLine = false;
                }
                else 
                {
                    $currentValue = [0, 0, 0, 0, 0, 0]; 
                    for ($c=1; $c < count($data); $c++) // Get current value
                    {
                        $value = $data[$c];
                        if ($c != 6)
                        {
                            $currentValue[$c] = round($value, 2); // 5 first elements are float
                        }
                        else
                        {
                            $currentValue[$c] = $value; // last element are int (volume)
                        }
                    }
                    $currentDate = DateTime::createFromFormat('Y-m-d', $data[0])->setTime(0, 0); // Get current date

                    if (count($histData) > 0) // If array is not empty
                    {
                        $lastDate = DateTime::createFromFormat('Y-m-d', array_key_last($histData))->setTime(0, 0); // Get last date stored
                        $lastValue = $histData[array_key_last($histData)]; // Get last value stored
                        
                        while ($lastDate->add(new DateInterval('P1D')) != $currentDate) // While last date + 1 day is not current date
                        {
                            $histData[$lastDate->format('Y-m-d')] = $lastValue; // Store last date + 1 day with last value
                        }
                        $histData[$currentDate->format('Y-m-d')] = $currentValue; // Finally store current date with current value
                    }
                    else
                        $histData[$data[0]] = $currentValue; // if array is empty store first date with first value
                }
            }
            fclose($handle);
        }

        return $histData;
    }

    /**
     * Return an array of historical data of the stock quote : 
     * - Date
     * - Adj Close (Adjusted close price adjusted for both dividends and splits)
     *
     * @param string $url, raw data of the html page
     * @return array, array of historical data of the stock quote
     */
    public static function parseHistoricalDataFromHTMLCurrency(string $url)
    {
        $firstLine = true;
        $histData = array();
        if (($handle = fopen($url, "r")) !== FALSE) 
        {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) 
            {
                if ($firstLine)
                {
                    $firstLine = false; // Remove column header
                }
                else 
                {
                    if (count($data) == 7)
                    {
                        $currentValue = round($data[5], 6);
                        if ($currentValue != 0)
                        {
                            $currentDate = DateTime::createFromFormat('Y-m-d', $data[0])->setTime(0, 0);
                            if (count($histData) > 0) // If array is not empty
                            {
                                $lastDate = DateTime::createFromFormat('Y-m-d', array_key_last($histData))->setTime(0, 0); // Get last date stored
                                $lastValue = $histData[array_key_last($histData)]; // Get last value stored
                                
                                while ($lastDate->add(new DateInterval('P1D')) != $currentDate) // While last date + 1 day is not current date
                                {
                                    $histData[$lastDate->format('Y-m-d')] = $lastValue; // Store last date + 1 day with last value
                                }
                                $histData[$currentDate->format('Y-m-d')] = $currentValue; // Finally store current date with current value
                            }
                            else
                                $histData[$data[0]] = $currentValue; // if array is empty store first date with first value
                        }
                    }
                    
                }
            }
            fclose($handle);
        }

        return $histData;
    }

    /**
     * Create URL link from Yahoo Finance on profil
     *
     * @return string, url create form shortname for profile YF
     */
    public static function createProfileLinkFromStock(string $shortName)
    {
        $url = self::YAHOO_FINANCE_BASE_URL
                .self::YAHOO_FINANCE_QUOTE
                .$shortName
                .self::YAHOO_FINANCE_PROFILE
                .$shortName;

        return $url;
    }

    /**
     * Create URL link from Yahoo Finance
     *
     * @return string, url create form shortname for YF
     */
    public static function createLinkFromStock(string $shortName)
    {
        $url = self::YAHOO_FINANCE_BASE_URL
                .self::YAHOO_FINANCE_QUOTE
                .$shortName;

        return $url;
    }

    public static function createHistDataLinkFromStock(string $shortName, DateTime $firstDate, DateTime $lastDate)
    {
        $url = self::YAHOO_FINANCE_BASE_URL
                .self::YAHOO_FINANCE_QUOTE
                .$shortName
                .self::YAHOO_FINANCE_HISTORY
                .self::YAHOO_FINANCE_PERIOD1
                .date_timestamp_get($firstDate)
                .self::YAHOO_FINANCE_PERIOD2
                .date_timestamp_get($lastDate)
                .self::YAHOO_FINANCE_END_PART;
        
        return $url;
    }

    /**
     * Create URL link from Yahoo Finance on historical data. Stock
     *
     * @param string $shortName
     * @param DateTime $firstDate
     * @param DateTime $lastName
     * @return string
     */
    public static function createHistDataDownloadLinkFromStock(string $shortName, DateTime $firstDate, DateTime $lastDate)
    {
        $url = self::YAHOO_FINANCE_BASE
                .$shortName
                .self::YAHOO_FINANCE_PERIOD1
                .date_timestamp_get($firstDate)
                .self::YAHOO_FINANCE_PERIOD2
                .date_timestamp_get($lastDate)
                .self::YAHOO_FINANCE_END_PART_DL;

        return $url;
    }

    /**
     * Create URL link from Yahoo Finance on historical data. Currency
     *
     * @param string $shortName
     * @param DateTime $firstDate
     * @param DateTime $lastName
     * @return string
     */
    public static function createHistDataDownloadLinkFromCurrency(string $shortName, DateTime $firstDate, DateTime $lastDate)
    {
        $url = self::YAHOO_FINANCE_BASE
                .'EUR'
                .$shortName
                .'=X'
                .self::YAHOO_FINANCE_PERIOD1
                .date_timestamp_get($firstDate)
                .self::YAHOO_FINANCE_PERIOD2
                .date_timestamp_get($lastDate)
                .self::YAHOO_FINANCE_END_PART_DL;

        return $url;
    }

    public static function getCurrentSharePriceFromStock(Stock $stock)
    {
        $url = self::createLinkFromStock($stock->getShortName());
        $html = self::getRawHTML($url);
        return self::parseSharePriceFromHTML($html);
    }

    public static function getCurrentExchangeRate(string $currency)
    {
        $url = self::createLinkFromStock('EUR'.$currency.'=X');
        $html = self::getRawHTML($url);
        return self::parseSharePriceFromHTML($html);
    }

    /**
     * Get a crawler with xpath filter
     *
     * @param string $html, raw data of the html page
     * @param string $filter, will filter the html with xpath
     * @return Crawler, a crawler that already filtered
     */
    private static function getCrawlerWithFilter(string $html, string $filter)
    {
        $crawler = new Crawler();
        $crawler->addHtmlContent($html);
        return $crawler->filterXPath($filter);
    }
}
<?php

namespace App\Utils;

class Financial
{
    const FINANCIAL_MAX_ITERATIONS = 128;
    const FINANCIAL_PRECISION = 1.0e-08;

    public static function xirr($values, $dates, $guess = 0.1) { 
        $x1 = 0.0;
        $x2 = $guess;
        $f1 = self::xnpv($x1, $values, $dates);  
        $f2 = self::xnpv($x2, $values, $dates); 
        for ($i = 0; $i < 128; ++$i) {
            if (($f1 * $f2) < 0.0) break;
            if (abs($f1) < abs($f2)) {
                $f1 = self::xnpv($x1 += 1.6 * ($x1 - $x2), $values, $dates);
            } else {
                $f2 = self::xnpv($x2 += 1.6 * ($x2 - $x1), $values, $dates);
            }
        } 

        $f = self::xnpv($x1, $values, $dates);
        if ($f < 0.0) {
            $rtb = $x1;
            $dx = $x2 - $x1;
        } else {
            $rtb = $x2;
            $dx = $x1 - $x2;
        }

        for ($i = 0;  $i < self::FINANCIAL_MAX_ITERATIONS; ++$i) {
            $dx *= 0.5;
            $x_mid = $rtb + $dx;
            $f_mid = self::xnpv($x_mid, $values, $dates);
            if ($f_mid <= 0.0) $rtb = $x_mid;

            if ((abs($f_mid) < self::FINANCIAL_PRECISION) || (abs($dx) < self::FINANCIAL_PRECISION)) return $x_mid;
        } 
    }

    private static function xnpv($rate, $values, $dates) {

        $valCount = count($values);  
        $xnpv = 0.0;
        for ($i = 0; $i < $valCount; ++$i) 
        { 

            $datediff = strtotime($dates[$i]) - strtotime($dates[0]);
            $datediff =  round($datediff / (60 * 60 * 24));  
            $xnpv += $values[$i] / pow(1 + $rate,$datediff / 365); 
        } 
        return $xnpv; 
    }
}
<?php

namespace App\Utils;

use DateTime;
use Financial;
use DateInterval;
use App\Entity\Stock;
use App\Entity\LinePf;
use App\Entity\Portefolio;
use App\Entity\Transaction;
use Psr\Log\LoggerInterface;
use App\Entity\HistDataPortefolio;
use App\Utils\TradingDataExtractor;
use Doctrine\Common\Persistence\ObjectManager;

class PortefolioFactory 
{
    /**
     * Create with transactions a portefolio and return it 
     *
     * @param array $transactions, has to be order by date
     * @param LoggerInterface $logger, can be null
     * @return Protefolio
     */
    public static function createPortefolio(Portefolio $portefolio, $transactions, LoggerInterface $logger)
    {
        self::info('Create and return the user complete portefolio');

        foreach ($transactions as $tr) {
            self::addTransactionToPortefolio($portefolio, $tr, $logger);
        }

        return $portefolio;
    }


    public static function addTransactionToPortefolio(ObjectManager $manager, Portefolio $portefolio, Transaction $tr, LoggerInterface $logger)
    {
        self::updateHistData($portefolio, $tr, $logger);
        switch($tr->getCategory())
        {
            case Transaction::CATEGORY_PURCHASE: 
                $portefolio = self::loadPurchase($tr, $portefolio, $logger);
            break;      
            case Transaction::CATEGORY_SELL: 
                $portefolio = self::loadSell($tr, $portefolio, $logger);
                break;      
            case Transaction::CATEGORY_PAYMENT: 
                $portefolio = self::loadPayment($tr, $portefolio, $logger);
                break;      
            case Transaction::CATEGORY_WITHDRAWAL: 
                $portefolio = self::loadWithdrawal($tr, $portefolio, $logger);
                break;      
            case Transaction::CATEGORY_DIVIDEND:
                $portefolio = self::loadDividend($tr, $portefolio, $logger);
                break;      
            case Transaction::CATEGORY_STOCKDIVIDEND: 
                $portefolio = self::loadStockDividend($tr, $portefolio, $logger);
                break;   
            case Transaction::CATEGORY_FEE:
                $portefolio = self::loadFee($tr, $portefolio, $logger);
            break;   
        }

        self::updateHistData($portefolio, $tr, $logger);
        self::persist($manager, $portefolio);
    }

    /**
     * Persist into database portefolio
     *
     * @param [type] $manager
     * @param [type] $portefolio
     * @return void
     */
    private static function persist($manager, $portefolio)
    {
        foreach ($portefolio->getLinesPf() as $line) {
            $manager->persist($line);
        }
        foreach($portefolio->getHistDataPortefolios() as $hist) {
            $manager->persist($hist);
        }
        $manager->persist($portefolio);
        $manager->flush();
    }

    /**
     * Agregate multiple portefolio in one non persisted new portefolio
     *
     * @param array $portefolios
     * @return void
     */
    public static function agregatePortefolios(array $portefolios, LoggerInterface $logger)
    {
        $logger->debug('Agregate '.count($portefolios).' portefolios in new one non persisted.');
        
        $portefolio = new Portefolio(new DateTime(), $logger);

        foreach($portefolios as $p)
        {
            $portefolio->setFs($p->getFs()); // Fs is not important, so we take the last one at the last iteration
            $portefolio->setCash($portefolio->getCash() + $p->getCash()); // Sum cash
            $portefolio->setCreateAt($p->getCreateAt() < $portefolio->getCreateAt() ? $p->getCreateAt() : $portefolio->getCreateAt()); // Get oldest date
            $portefolio->setUser($p->getUser()); // User is not important, so we take the last one at the last iteration
            $linesPf = $p->getLinesPf(); // Get lines of pf...
            foreach($linesPf as $line) $portefolio->addLinesPf($line); //... and add them to the new portefolio
            $portefolio->setPayments($portefolio->getPayments() + $p->getPayments()); // Sum payments
            self::agregateLinesPf($portefolio, $p); // Update histdataportefolio
            $paymentsWithdrawalTr = $p->getPaymentsWithdrawalTr();
            foreach($paymentsWithdrawalTr as $pwt) $portefolio->addPaymentsWithdrawalTr($pwt);
        }

        return $portefolio;
    }

    /**
     * Agregate lines of portefolio into the new one
     *
     * @param Portefolio $newPortefolio
     * @param Portefolio $portefolio
     * @return void
     */
    private static function agregateLinesPf(Portefolio $newPortefolio, Portefolio $portefolio)
    {
        foreach($portefolio->getHistDataPortefolios() as $hist)
        {
            $existingHist = self::searchLineByDateInPf($newPortefolio, $hist->getCreateAt());
            if (is_null($existingHist)) $newPortefolio->addHistDataPortefolio($hist);
            else 
            {
                self::getDataForPortefolio($portefolio, DateTime::createFromFormat('Y-m-d', $hist->getCreateAt()->format('Y-m-d'))); // Update data
                self::updateHistDataToPf($portefolio, $existingHist->getCreateAt());
            }
        }
    }

    /**
     * Search a historical data in portefolio filter date
     *
     * @param Portefolio $portefolio
     * @param DateTime $date
     * @return void
     */
    private static function searchLineByDateInPf(Portefolio $portefolio, DateTime $date)
    {
        foreach($portefolio->getHistDataPortefolios() as $hist)
        {
            if ($hist->getCreateAt()->setTime(0, 0) == $date->setTime(0, 0)) return $hist;
        }
        return null;
    }

    /**
     * Update a historical data to portefolio
     *
     * @param Portefolio $pf
     * @param DateTime $date
     * @return void
     */
    private static function updateHistDataToPf(Portefolio $pf, DateTime $date)
    {
        foreach($pf->getHistDataPortefolios() as $key => $hist)
        {
            if ($hist->getCreateAt()->format('Y-m-d') == $date->format('Y-m-d'))
            {
                $pf->removeHistDataPortefolio($hist); // Remove old line
                $hist->setValuation($pf->getValuation()) // Set new valuation 
                     ->setCash($pf->getCash()) // Set new cash value
                     ->setPayments($pf->getPayments()) // Set new cash value
                     ->setXirr($pf->getXirr()) // Set new cash value
                     ->setGlobalXirr($pf->getGlobalXirr()); // Set new cash value
                $pf->addHistDataPortefolio($hist); // Add new line
            break;
            }
        }
    }

    /**
     * Get total valuation of the portefolio given by argument
     *
     * @param Portefolio $portefolio
     * @param LoggerInterface $logger
     * @return void
     */
    public static function getValuation(Portefolio $portefolio, LoggerInterface $logger = null)
    {
        $valuation = 0;
        foreach ($portefolio->getLinesPf() as $line) 
        {
            if ($line->getStatus() == true) // Valuation of the only open lines
            {
                $valuation += $line->getAmount(); // Update valuation
            }
        }
        return $valuation;
    }

    /**
     * Update Share price of each stock in open line of the portefolio
     *
     * @param Portefolio $portefolio
     * @param LoggerInterface $logger
     * @return void
     */
    public static function updateSharePrice(Portefolio $portefolio, DateTime $lastDate = null, LoggerInterface $logger = null)
    {
        foreach($portefolio->getLinesPf() as $line)
        {
            if ($line->getStatus() == true) // Update share price of the open lines
            {
                $histData = $line->getStock()->getHistDataStocks()->toArray(); // Get historical data of stock
                $sharePrice = 0;

                if (is_null($lastDate) || $lastDate->format('Y-m-d') == date('Y-m-d'))
                {
                    $sharePrice = round($histData[array_key_last($histData)]->getAdjClose(), 2); // Get last element
                }
                else
                {
                    $sharePrice = TradingDataExtractor::getValueFromDateInHistData($histData, $lastDate);
                }
                if (!is_null($logger)) $logger->debug('Share price : '.$sharePrice.' at '.$lastDate->format('Y-m-d').'.');

                $line->setSharePrice($sharePrice); // Update share price
            }
        }

        return $portefolio;
    }

    /**
     * Update Share unit cost price of each stock in open line of the portefolio
     *
     * @param Portefolio $portefolio
     * @param LoggerInterface $logger
     * @return void
     */
    public static function updateUCP(Portefolio $portefolio, LoggerInterface $logger = null)
    {
        foreach($portefolio->getLinesPf() as $line)
        {
            if ($line->getStatus() == true) // Update ucp of the open lines
            {
                $chargedAmount = 0;
                $quantity = 0;

                foreach($line->getTransactions() as $tr) // Loop through all transactions that belong to the line
                {
                    if ($tr->getCategory() == Transaction::CATEGORY_PURCHASE) // Fetch just the purchase transactions
                    {
                        $quantity += $tr->getQuantity();
                        if ($line->getStock()->getCurrency()->getShortName() == 'EUR') // If it's in euro get charged amount
                        {
                            $chargedAmount += $tr->getDebit(); // Get charged amount
                        }
                        else // Other currency, ucp is not with charge like brokerage or fee
                        {
                            $chargedAmount += ($tr->getPrice() * $tr->getQuantity()); // get amount without fee or brokerage
                        }
                    }
                    elseif ($tr->getCategory() == Transaction::CATEGORY_SELL) // If purchase, remove quantity and amount
                    {
                        $quantity -= $tr->getQuantity();
                        if ($quantity == 0) // All is sold
                        {
                            $chargedAmount = 0;
                        }
                        else // Part of stocks is sold
                        {
                            if ($line->getStock()->getCurrency()->getShortName() == 'EUR') // If it's in euro get charged amount
                            {
                                $chargedAmount -= $tr->getCredit(); // Get charged amount
                            }
                            else // Other currency, ucp is not with charge like brokerage or fee
                            {
                                $chargedAmount -= ($tr->getPrice() * $tr->getQuantity()); // get amount without fee or brokerage
                            }
                        }
                    }
                }
                $ucp = round($chargedAmount / $quantity, 2); // Calculate ucp
                $line->setUcp($ucp); // Update ucp in line
            }
        }

        return $portefolio;
    }

    /**
     * Update the latent gain or loss into portefolio for each open line
     *
     * @param Portefolio $portefolio
     * @param LoggerInterface $logger
     * @return void
     */
    public static function updateLatentGl(Portefolio $portefolio, LoggerInterface $logger = null)
    {
        foreach($portefolio->getLinesPf() as $line)
        {
            if ($line->getStatus() == true) // Update latentGl of the open lines
            {
                $fee = 0; // For stock not in euro, add fees on amount
                $exchange = 1;
                if ($line->getStock()->getCurrency()->getShortName() != 'EUR') // Test if stock is not in euro
                {
                    foreach($line->getTransactions() as $tr)
                    {
                        if ($tr->getCategory() == Transaction::CATEGORY_PURCHASE) // Purchase, we add
                        {
                            $fee += ($tr->getBrokerage() + $tr->getFee());
                        }
                        elseif ($tr->getCategory() == Transaction::CATEGORY_SELL) // Sell, we remove
                        {
                            $fee -= ($tr->getBrokerage() + $tr->getFee());
                        }
                    }

                    $histDataCurrency = $line->getStock()->getCurrency()->getHistDataCurrencies()->toArray(); // Get historical data of currency
                    $exchange = $histDataCurrency[array_key_last($histDataCurrency)]->getAdjClose(); // Get last exchange rate
                }

                $payedAmount = ($line->getUcp() * $line->getQuantity()) / $exchange; // Calculate the payed amount 
                $latentGl = ($line->getAmount() + $fee) - $payedAmount; // Calculate latent gain or loss
                $latentGlP = round(($latentGl / $payedAmount), 2); // Calculate latent gain or loss percent
                $line->setLatentGl($latentGl); // Update latent gain or loss
                $line->setLatentGlP($latentGlP); // Update latent gain or loss percent
            }
        }

        return $portefolio;
    }

    public static function updateAmount(Portefolio $portefolio, DateTime $lastDate = null, LoggerInterface $logger = null)
    {
        
        foreach($portefolio->getLinesPf() as $line)
        {
            if ($line->getStatus() == true) // Update amount of the open lines
            {
                $currency = 1;
                if ($line->getStock()->getCurrency() != 'EUR') // If stock currency is not euro
                {
                    $histDataCurrency = $line->getStock()->getCurrency()->getHistDataCurrencies()->toArray(); // Get historical data of currency
                    
                    if (is_null($lastDate) || $lastDate->format('Y-m-d') == date('Y-m-d'))
                    {
                        $currency = $histDataCurrency[array_key_last($histDataCurrency)]->getAdjClose(); // Get last exchange rate
                    }
                    else
                    {
                        $currency = TradingDataExtractor::getValueFromDateInHistData($histDataCurrency, $lastDate);
                    }

                    if (!is_null($logger)) $logger->debug('currency : '.$currency);
                }
                if ($currency != 0)
                {
                    $amount = round(($line->getQuantity() * $line->getSharePrice()) / $currency, 2); // Update amount with last exchange rate with 2 digits
                    $line->setAmount($amount);
                }
            }
        }

        return $portefolio;
    }

    /**
     * Get Data for xirr function !
     * Be carefull, xirr function use payments as negative and withdrawal as positive.
     * For more information, see this url : http://www.objectifrente.com/patrimoine/comment-calculer-la-rentabilite-de-votre-portefeuille
     *
     * @param [type] $portefolios
     * @param LoggerInterface $logger
     * @return void
     */
    public static function getXirrData($portefolios, $total, LoggerInterface $logger = null)
    {
        $xirrData = []; // Data for xirr function. Date is the key, amount is the value

        foreach ($portefolios as $p) // Loop through all portefolios
        {
            foreach ($p->getPaymentsWithdrawalTr() as $tr) // Loop through all payment or withdrawal transactions
            {
                switch($tr->getCategory())
                {
                    case Transaction::CATEGORY_PAYMENT:
                        $date = $tr->getCreateAt()->format('Y-m-d');
                        if (array_key_exists($date, $xirrData))
                        {
                            $xirrData[$date] -= $tr->getCredit(); // If date already stored, add amount
                        }
                        else
                        {
                            $xirrData[$date] = -$tr->getCredit(); // If date does not exist, set it with amount
                        }
                    break;
                    case Transaction::CATEGORY_WITHDRAWAL:
                        $date = $tr->getCreateAt()->format('Y-m-d');
                        if (array_key_exists($date, $xirrData))
                        {
                            $xirrData[$date] += $tr->getDebit(); // If date already stored, add amount
                        }
                        else
                        {
                            $xirrData[$date] = $tr->getDebit(); // If date does not exist, set it with amount
                        }
                    break;
                }
            }
        }

        ksort($xirrData); // Sort the array by the keys

        $xirrValues = array_values($xirrData); // Fetch the values
        $xirrKeys = array_keys($xirrData); // Fetch the keys
        array_push($xirrKeys, date_format(new DateTime(), 'Y-m-d')); // Add it the current date
        array_push($xirrValues, $total); // add it the current total of portefolios
        $xirr = Financial::xirr($xirrValues, $xirrKeys); // Get the xirr of portefolios

        return $xirr;
    }

    /**
     * Get the global xirr.
     *
     * @param [type] $xirr
     * @param [type] $firstDate
     * @param [type] $currentDate
     * @return void
     */
    public static function getGlobalXirrData($xirr, $firstDate, $currentDate)
    {
        $days = $firstDate->diff($currentDate)->days;
        $global = pow((1+$xirr), ($days/365)) - 1;
        return $global;
    } 

    /**
     * Load a purchase transaction into the portefolio
     *
     * @param Transaction $tr
     * @return void
     */
    private static function loadPurchase(Transaction $tr, Portefolio $portefolio, LoggerInterface $logger = null)
    {
        $line = self::getLineInPortefolio($portefolio, $tr->getStock()); // Search line in portefolio

        if (is_null($line)) // Line not found
        {
            $line = new LinePf();
            $line->setStock($tr->getStock()) // Add a stock
                 ->setQuantity($tr->getQuantity()) // Add a quantity
                 ->setSharePrice($tr->getPrice()); // Add a price
        }
        else
        {
            $portefolio->removeLinesPf($line); // Remove the old line
            $line->setQuantity($line->getQuantity() + $tr->getQuantity()) // Update quantity
                 ->setSharePrice($tr->getPrice()); // Update price
        }

        $line->addTransaction($tr); // Add transaction in all case
        $line->setStatus(true); // Set the status line as OPEN
        $portefolio->addLinesPf($line); // Then add the new line into the portefolio
        $portefolio->setCash($portefolio->getCash() - $tr->getDebit()); // Update cash in portefolio

        return $portefolio;
    }

    /**
     * Load a sell transaction
     *
     * @param Transaction $tr
     * @return void
     */
    private static function loadSell($tr, Portefolio $portefolio, LoggerInterface $logger = null)
    {
        $line = self::getLineInPortefolio($portefolio, $tr->getStock()); // Search line in portefolio

        if (!is_null($line)) // Test, if line is present in portefolio
        {
            $portefolio->removeLinesPf($line); // Remove the old line
            $line->setQuantity($line->getQuantity() - $tr->getQuantity()); // Update quantity
            if ($line->getQuantity() == 0) $line->setStatus(false); // Set the status line as CLOSE if all is sold
            $line->setSharePrice($tr->getPrice()); // Update price
            $line->addTransaction($tr); // Add transation
            $portefolio->addLinesPf($line); // Add the new line into the portefolio
            $portefolio->setCash($portefolio->getCash() + $tr->getCredit()); // Update cash in portefolio
        }

        return $portefolio;
    }

    /**
     * Load a payment transaction.
     * If it's the first transaction saved, then the portefolio set its variables (user, financial service and createAt)
     *
     * @param Transaction $tr
     * @return void
     */
    private static function loadPayment(Transaction $tr, Portefolio $portefolio, LoggerInterface $logger = null)
    {
        $amount = $tr->getCredit();

        self::info('Load a payment transaction, amount : '.$amount, $logger);

        if (is_null($portefolio->getFs())) // First transaction, so we set variables in portefolio
        {
            $portefolio->setFs($tr->getFinancialService())
                       ->setCash($amount)
                       ->setPayments($amount)
                       ->setCreateAt($tr->getCreateAt())
                       ->setUser($tr->getUser());
        }
        else 
        {
            $portefolio->setCash($portefolio->getCash() + $amount)
                       ->setPayments($portefolio->getPayments() + $amount);
        }

        $portefolio->addPaymentsWithdrawalTr($tr);

        return $portefolio;
    }

    /**
     * Load a withdrawal transaction
     *
     * @param Transaction $tr
     * @return void
     */
    private static function loadWithdrawal(Transaction $tr, Portefolio $portefolio, LoggerInterface $logger = null)
    {
        $amount = $tr->getDebit();

        self::info('Load a withdrawal transaction, amount : '.$amount, $logger);

        if (!is_null($portefolio->getFs())) // First transaction impossible, first transaction has to be a payment
        {
            $portefolio->setCash($portefolio->getCash() - $amount);
        }

        $portefolio->addPaymentsWithdrawalTr($tr);
        
        return $portefolio;
    }

    /**
     * Load a fee transaction
     *
     * @param Transaction $tr
     * @return void
     */
    private static function loadFee(Transaction $tr, Portefolio $portefolio, LoggerInterface $logger = null)
    {
        $amount = $tr->getDebit();

        self::info('Load a fee transaction, amount : '.$amount, $logger);

        if (!is_null($portefolio->getFs())) // First transaction impossible, first transaction has to be a payment
        {
            $portefolio->setCash($portefolio->getCash() - $amount);
        }
        
        return $portefolio;
    }

    private static function loadStockDividend($tr, Portefolio $portefolio, LoggerInterface $logger = null)
    {
        return self::loadPurchase($tr, $portefolio, $logger);
    }

    /**
     * Load a dividend transaction
     *
     * @param [type] $tr
     * @return void
     */
    private static function loadDividend($tr, Portefolio $portefolio, LoggerInterface $logger = null)
    {
        $line = self::getLineInPortefolio($portefolio, $tr->getStock()); // Search line in portefolio

        if (!is_null($line)) // Test, if line is present in portefolio
        {
            $portefolio->removeLinesPf($line); // Remove the old line
            $line->addTransaction($tr); // Add transation
            $portefolio->addLinesPf($line); // Add the new line into the portefolio
            $portefolio->setCash($portefolio->getCash() + $tr->getCredit()); // Update cash in portefolio
        }

        return $portefolio;
    }

    /**
     * Search and fetch line of a stock in a portefolio.
     *
     * @param Portefolio $portefolio
     * @param Stock $stock
     * @return LinePf if found or null otherwise
     */
    private static function getLineInPortefolio(Portefolio $portefolio, Stock $stock)
    {
        foreach ($portefolio->getLinesPf() as $key => $line) {
            if ($line->getStock() == $stock)
                return $line;
        }

        return null;
    }

    public static function updateHistData($portefolio, $tr, $logger, $now = null)
    {
        $logger->debug('Update historical data in portefolio');
        if (count($portefolio->getHistDataPortefolios()) > 0) // There is already some data
        {
            $logger->debug('There are '.count($portefolio->getHistDataPortefolios()).' lines of historical data in portefolio');

            $lastDate = DateTime::createFromFormat('Y-m-d', $portefolio->getHistDataPortefolios()->last()->getCreateAt()->format('Y-m-d'))->setTime(0, 0); // Get last date stored
            $currentDate = is_null($now) ? DateTime::createFromFormat('Y-m-d', $tr->getCreateAt()->format('Y-m-d'))->setTime(0, 0) : $now->setTime(0, 0); // Get current date to store
        
            if ($lastDate == $currentDate) // Modifying an existing line
            {
                self::getDataForPortefolio($portefolio, DateTime::createFromFormat('Y-m-d', $currentDate->format('Y-m-d')), false, $logger); // Update data
                self::updateHistDataToPf($portefolio, $currentDate);
                $logger->debug($lastDate->format('Y-m-d').' and '.$currentDate->format('Y-m-d').' are identicals, we modify existing historical line.');
            }
            else
            {
                $currentDatePlusOneDay = DateTime::createFromFormat('Y-m-d', $currentDate->format('Y-m-d'))->setTime(0, 0);
                while($lastDate->add(new DateInterval('P1D')) != $currentDatePlusOneDay) // We iterate until the current day !
                {
                    self::getDataForPortefolio($portefolio, DateTime::createFromFormat('Y-m-d', $lastDate->format('Y-m-d')), false, $logger); // Update data
                    self::createHistDataInPf($lastDate, $portefolio->getValuation(), $portefolio->getCash(), $portefolio->getPayments(), 
                        $portefolio->getXirr(), $portefolio->getGlobalXirr(), $portefolio); // Create hist line from updated data
                    $logger->debug('Creating line at '.$lastDate->format('Y-m-d').' in portefolio.');
                }
            }
        }
        else
        {
            self::createHistDataInPf($tr->getCreateAt(), $tr->getCredit(), $tr->getCredit(), $tr->getCredit(), 0, 0, $portefolio);
            $logger->debug('Add first line of historical data in portefolio !');
        }
    }

    /**
     * Create and attach to portefolio a historical data
     *
     * @param [type] $createAt
     * @param [type] $valuation
     * @param [type] $cash
     * @param [type] $payments
     * @param [type] $xirr
     * @param [type] $globalXirr
     * @param [type] $portefolio
     * @return void
     */
    private static function createHistDataInPf($createAt, $valuation, $cash, $payments, $xirr, $globalXirr, $portefolio)
    {
        $histData = new HistDataPortefolio();
        $histData->setCreateAt(DateTime::createFromFormat('Y-m-d', $createAt->format('Y-m-d'))->setTime(0, 0))
                 ->setValuation($valuation)
                 ->setCash($cash)
                 ->setPayments($payments)
                 ->setXirr($xirr)
                 ->setGlobalXirr($globalXirr)
                 ->setPortefolio($portefolio);
        $portefolio->addHistDataPortefolio($histData);
    }

    /**
     * Generate data for portefolio
     * 
     *
     * @param [type] $arrayPf, Collection of portefolios
     * @return array 
     */
    public static function getDataForPortefolio($portefolio, $currentDate, bool $ihm = false, LoggerInterface $logger = null)
    {
        $valuation = 0; // Valuation of assets
        $cash = 0; // Cash
        $total = 0; // Total of cash + valuation
        $startDate = new DateTime();

        $portefolio = self::updateSharePrice($portefolio, $currentDate, $logger);
        $portefolio = self::updateAmount($portefolio, $currentDate, $logger);
        if ($ihm) // If data are for IHM
        {
            $portefolio = self::updateUCP($portefolio);
            $portefolio = self::updateLatentGl($portefolio, $logger);
        }
        $valuation += self::getValuation($portefolio);
        $cash += $portefolio->getCash(); 
        if ($portefolio->getCreateAt() < $startDate) $startDate = $portefolio->getCreateAt();

        $total = $valuation + $cash;

        $xirr = self::getXirrData([$portefolio], $total);
        $globalXirr = self::getGlobalXirrData($xirr, $startDate, $currentDate);

        $portefolio->setValuation($total)
                   ->setXirr($xirr ?? 0)
                   ->setGlobalXirr($globalXirr ?? 0);
        
        $portefolio->setLastValuation($currentDate);
    }

    public static function getDataForPortefolios($arrayPf, $currentDate, bool $ihm = false, LoggerInterface $logger = null)
    {
        $valuation = 0; // Valuation of assets
        $cash = 0; // Cash
        $total = 0; // Total of cash + valuation
        $payments = 0; // Total of payments
        $startDate = new DateTime();

        foreach ($arrayPf as $pf) {
            $pf = PortefolioFactory::updateSharePrice($pf);
            $pf = PortefolioFactory::updateAmount($pf);
            $pf = PortefolioFactory::updateUCP($pf);
            $pf = PortefolioFactory::updateLatentGl($pf, $logger);
            $valuation += PortefolioFactory::getValuation($pf);
            $cash += $pf->getCash(); 
            $payments += $pf->getPayments();
            if ($pf->getCreateAt() < $startDate) $startDate = $pf->getCreateAt();
        }
        $total = $valuation + $cash;

        $xirr = PortefolioFactory::getXirrData($arrayPf, $total);
        $globalXirr = PortefolioFactory::getGlobalXirrData($xirr, new DateTime('2018-04-27'), new DateTime);

        return [$valuation, $cash, $total, $payments, $currentDate, $xirr, $globalXirr, $startDate];
    }

    private static function info(string $log, LoggerInterface $logger = null)
    {
        if (!is_null($logger))
            $logger->info($log.' | '.__METHOD__);
    }

    private static function debug(string $log, LoggerInterface $logger = null)
    {   
        if (!is_null($logger))
            $logger->debug($log.' | '.__METHOD__);
    }
}
<?php

namespace App\Tests;

use DateTime;
use App\Entity\Stock;
use App\Utils\YFParser;
use PHPUnit\Framework\TestCase;

class YFParserTest extends TestCase
{
    /**
     * 
     *
     * @return void
     */
    public function testYahooFinanceParser()
    {
        // Test URL on profile
        $vinciURLProfile = YFParser::createProfileLinkFromStock('DG.PA');
        $this->assertEquals($vinciURLProfile, "https://finance.yahoo.com/quote/DG.PA/profile?p=DG.PA");

        // Test on Sectors
        $vinciRawHTML = YFParser::getRawHTML($vinciURLProfile);
        $this->assertNotNull($vinciRawHTML, "Unable to get raw HTML from URL given");
        $vinciProfileArray = YFParser::parseProfileFromHTML($vinciRawHTML);
        $this->assertEquals([Stock::SECTOR => "Industrials", Stock::ACTIVITY_SECTOR => "Engineering & Construction"], 
                            $vinciProfileArray, "Unable to fetch correct data for profile");

        // Test URL on historical data
        $vinciURLHistData = YFParser::createHistDataLinkFromStock('DG.PA', new DateTime('28-03-2020'), new DateTime('02-04-2020'));
        $this->assertEquals($vinciURLHistData, 'https://finance.yahoo.com/quote/DG.PA/history?period1=1585353600&period2=1585785600&interval=1d&filter=history&frequency=1d');

        // Test on historical data
        $vinciRawHTML = YFParser::getRawHTML($vinciURLHistData);
        $this->assertNotNull($vinciRawHTML, "Unable to get raw HTML from URL given");
        // $histData = YFParser::parseHistoricalDataFromHTML($vinciRawHTML);
        // $this->assertEquals(count($histData), 3);

        // Test Stock parser
    }
}
